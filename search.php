<?php

if (!isset($_GET['Select']) || $_GET['Select'] == "")
{
   header ("Location: http://www.google.com/search?hl=en&lr=UTF-8&oe=UTF-8&q=site%3A".$_SERVER['SERVER_NAME']."+".urlencode($_GET['q']));
   exit ();
}

switch ($_GET['Select'])
{
  case "kdelook":
    header ("Location: http://www.kde-look.org/content/search.php?search=Search&name=".urlencode($_GET['q']));
    exit ();

  case "kdeapps":
    header ("Location: http://www.kde-apps.org/content/search.php?search=Search&name=".urlencode($_GET['q']));
    exit ();

  case "kdedoc":
    header ("Location: http://docs.kde.org/cgi-bin/desktopdig/search.cgi?collection=en&include=development&mode=all&q=".urlencode($_GET['q']));
    exit ();

  default:
    header ("Location: http://www.google.com/search?hl=en&lr=UTF-8&oe=UTF-8&q=site%3A".$_GET['Select']."+".urlencode($_GET['q']));
    exit ();
}

?>
