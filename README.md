# Capacity (a.k.a media folder)

Capacity is a web framework developed to provide an unified theme and common
functionalities to KDE websites. This framework is now deprecated and new
KDE websites should use the [KDE Jekyll theme](https://invent.kde.org/websites/jekyll-kde-theme/),
or if it is not possible, the [KDE bootstrap theme](https://invent.kde.org/websites/aether-sass)
hosted in KDE CDN [cdn.kde.org/aether-devel](https://cdn.kde.org/aether-devel).
