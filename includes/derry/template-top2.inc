<?php 
    if (function_exists('function_to_get_sidebar_content'))
      $sidebar_content = function_to_get_sidebar_content();

    if( isset($sidebar_content) && $sidebar_content != "" ) { ?>
        <div id="sidebar">
            <?php global $sidebar_content; echo $sidebar_content; ?>
        </div>
    <?php } ?>
    <div id="module">
    </div>
