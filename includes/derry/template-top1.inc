<?php // This file contains content between the top of the html body and the heading
    global $document_root, $applicationIndex;
?>
<script type="text/javascript" src="/media/javascripts/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/media/javascripts/plasmaMenu.min.js"></script>
	<script type="text/javascript" src="/media/javascripts/jquery.cycle.all.min.js"></script>
	<script type="text/javascript" src="/media/javascripts/searchbox.js"></script>
	<script type="text/javascript" src="/media/javascripts/keynav.js"></script>

	<script type="text/javascript">

$(document).ready(function(event) {
 	$('a#top').click(function() {
 		$('body,html').animate({scrollTop:0},400);
 	});
	$('body').click(function() {
 		$('#search_result_list > li[class~=result_noindex_shown]').each(function(index, item) {
			jQuery(item).removeClass();
			jQuery(item).addClass("result_noindex_hidden");
		});
 	});
    $('#search').text('');
	$('#search').liveUpdate('#search_result_list');
	$('#search').click(function(e){
		e.stopPropagation();
		if (jQuery(this).val().length > 0) {
			jQuery(this).trigger('keyup');
		}
	});
	$('#search_result_list > li[class~=result_noindex_shown]').keynav('result_with_focus','result_without_focus');
	
	$('#search_result_list > li').each(function(index, item) {
		var jItem = jQuery(item);
		jItem.click(function() {
			window.location.href = jItem.children('a').attr('href');
			return false;
		});
	});
});

</script>
    <div class="root">
    	<div class="header">
			<h1 <?php if ($site_locale != "en") echo "class='back_non_english'" ?>><a href="/"><img src="/images/kdedu-logo.png" title="The KDE Education Project" alt="KDEdu Logo" height="92px" width="174px"/></a></h1>
			<div class="search">
				<?php if ($site_locale == "en") echo '<label for="search">Search </label>'; ?>
				<input type="text" placeholder="<?php if ($site_locale != "en") i18n("Search"); ?>" id="search"/>
			</div>
			<div class="search_result">
			<ul id="search_result_list">
				<?php
					foreach( $applicationIndex->getAllApplications() as $application ) {
						echo "<li class=\"result_noindex_hidden\">\n";
							echo "<img class=\"result_menubox_icon\" alt=\"\" src=\"/images/icons/".$applicationIndex->getFileName( $application )."_22.png\" />";
							echo "<a class=\"result_menubox_space\" href=\"/applications/all/".$applicationIndex->getFileName( $application )."\">".$applicationIndex->getName( $application )."</a>";
						echo "</li>";
					}
				?>
			</ul>
			</div>
			<?php
			  $template_menulist1 = "<li>";  // before the menu section title
			  $template_menulist2 = "<div class=\"sub\">";  // between the menu section title and the list of pages
			  $template_menulist3 = "</div></li>"; // after the list of pages

			  $template_menusublist1 = ""; // before each link to a (sub)page with shown (sub)subpages
			  $template_menusublist2 = ""; // between the link to a (sub)page and its list of (sub)subpages
			  $template_menusublist3 = ""; // after each list of subpages

			  $template_menuitem1 = ""; // before each link to a (sub)page without shown (sub)subpages
			  $template_menuitem2 = ""; // after each link to a (sub)page without shown (sub)subpages

			  if ($site_menus > 0 && $templatepath == "derry/") 
			  {
				if (isset($site) && $site == "developer")
				{
					echo '<div class="menu_box" onmouseover="javascript: $(\'.sub\').show(\'slow\');">
							<ul>';
							
					echo $template_menulist1."\n";
					$kde_menu->currentMenu();
					echo $template_menulist3."\n";

					if ($kde_menu->activeSection () != 0) {
						echo $template_menulist1."\n";
						$kde_menu->currentMenu(true);
						echo $template_menulist3."\n";
					}
					
					echo '</div>';
				}
				else {
					$plasmaMenu->showHtml();
				}
			  }
			?>
		</div>
	<div class="content">
	
    <div id="main">

		<div class="toolbox">
    		 <?php  include  $templatepath.'toolbox.inc'; ?>
		</div>
