<?php
header("Content-type: text/css");
?>

/* Eric Meyer CSS Reset */
html, body, div, span, applet, object, iframe,h1, h2, h3, h4, h5, h6, p, blockquote, pre,a, abbr, acronym, address, big, code,del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, legend, table, caption, tbody, tfoot, thead, tr, th, td, hr
{
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	vertical-align: baseline;
	background: transparent;
}

/*Rule por use EM font-size instead PX*/

html{
	font-size: 62.5%;
	background: #505050;
   	height: 100%;
}

/*Basic elements style*/

body { line-height: 1; font-size: 1.3em; text-align: center; 
	font-family: "Liberation Sans","Bitstream Vera Sans", Verdana, Arial, sans-serif;
	background: #fff url('../../images/background.png') repeat left top;
    color: #666666;}
ol, ul { list-style: none; }
blockquote, q {	quotes: none; }
blockquote:before, blockquote:after,q:before, q:after {	content: ''; content: none; }



/* remember to define focus styles! */
:focus { outline: 0; }

/* remember to highlight inserts somehow! */
ins { text-decoration: none; }
del { text-decoration: line-through; }

/* tables still need 'cellspacing="0"' in the markup */
table { border-collapse: collapse; border-spacing: 0; }


/*Formating and setups*/

.root {text-align: left; 
	background: url('../../images/bg-grunge.png') repeat-x center top;
	min-height: 440px;
	}
/*Header style*/

.header{
	padding-top: 25px;
}

.header h1{
	float: left;
	padding-left: 20px;
	position: relative;
	height: 92px;
}

.header h1:hover:after{
	content: ' ';
	background: url('../../../images/back-home.png') no-repeat left top;
	display: block;
	height: 53px;
	width: 153px;
	position: absolute;
	right: -125px;
	top: 20px;
}

.header .search{
	float: right;
	margin-top: 30px;
	margin-right: 20px;
}

.header .search label{
	text-indent: -99999999px;
	background: url('../../images/header/label-search.png') no-repeat left center;
	display: block;
	width: 90px;
	height: 30px;
	float: left;
}

.header .search input{
	background: url('../../images/header/input-search.png') no-repeat left top;
	border:none;
	padding: 8px 8px 7px 30px;
	color: #2d4e63;
	width: 97px;
}

.header .search_result{
	position: absolute;
	z-index: 200;
	right: 0px;
	top: 95px;
	width: 180px;
	background: #f2f3f3;
}	
/*.header .search_result ul li.result_noindex_shown:nth-child(2n+2){
	background: #e7eaec;
	border-top: 1px dotted #dde8f2;
	border-bottom: 1px dotted #dde8f2;
}*/

.header .search_result ul{
	-moz-box-shadow: 0 2px 5px rgba(0,0,0, .3), 0px 1px 0px #CED1DB inset;
	-webkit-box-shadow: 0 2px 5px rgba(0,0,0, .3), 0px 1px 0px #CED1DB inset;
	-khtml-box-shadow: 0 2px 5px rgba(0,0,0, .3), 0px 1px 0px #CED1DB inset;
	box-shadow: 0 2px 5px rgba(0,0,0, .3), 0px 1px 0px #CED1DB inset;
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	-khtml-border-radius: 8px;
	border-radius: 8px;
}

.header .search_result li{
	border-bottom: 1px dotted #CED1DB;
}
.header .search_result li img{
	/*display: none;*/
	float: left;
	width: 15px;
	margin-left: 10px;
	padding: 12px 5px;
}
.header .search_result li a{
	text-decoration: none;
	color: #0A71B4;
	display: block;
	padding: 13px 30px 13px 39px;
}

.header .search_result li a:hover{
	color: #fff;
	background: #0A71B4 url("../../../images/search_result-over.png") repeat-x scroll left top;
	text-shadow: 1px 1px 1px rgba(0,0,0, .5);
}

 .result_noindex_hidden   {
		display: none;
}
.result_noindex_shown{
		display:  list-item;
}

.header .search_result li.result_with_focus a{
		color: white;
		background: #0A71B4 url("../../../images/search_result-over.png") repeat-x scroll left top;
		text-shadow: 1px 1px 1px rgba(0, 0, 0, .5);
}
 .result_without_focus
	{
	}
.header .search_result li.result_noindex_first a{
	-moz-border-radius: 8px 8px 0px 0px;
	-webkit-border-radius: 8px 8px 0px 0px;
	-khtml-border-radius: 8px 8px 0px 0px;
	border-radius: 8px 8px 0px 0px;
}

.header .search_result li.result_noindex_last a{
	-moz-border-radius: 0px 0px 8px 8px;
	-webkit-border-radius: 0px 0px 8px 8px;
	-khtml-border-radius: 0px 0px 8px 8px;
	border-radius: 0px 0px 8px 8px;
}
.header .search_result li.result_noindex_first.result_noindex_last a{
	-moz-border-radius: 8px;
	-webkit-border-radius: 8px;
	-khtml-border-radius: 8px;
	border-radius: 8px;
}

.header .toolbox .toolboxtext { padding-top: 10px; }
.header .toolbox div {float: left; width: auto;padding: 0 10px 0 10px;color: #888; text-shadow: #fff 0px 0px 3px; text-decoration:none; font-size: 12px; font-weight:normal;}
.header .toolbox #location {min-width:370px;}
.header .toolbox #location li {float: left; width: auto; padding: 0 0 0 10px;background-image:url('../../images/crumb.arrow.png');
					    background-repeat:no-repeat;
					    background-position:0 2px}
.header .toolbox #location li a{color: #666; text-shadow: #fff 0px 0px 3px; text-decoration:none; font-size: 12px; font-weight:normal;}
.header .toolbox #location li a[href]:hover{text-decoration:underline}
.header .toolbox #location li:first-child {background-image:none}
.header .menu_box { display: table; text-align: left; width: auto; min-height:160px; margin: 0 auto; text-shadow: #fff 0px 0px 3px; }
.header .menu_box li { float: left; margin: 3px 14px }
.header .menu_box li a {text-decoration: none; margin: 0px 10px}
.header .menu_box li h2 a{color: #555; font-size: 14px; font-weight:normal; padding-left:10px; text-transform:uppercase}
.header .menu_box li ul { padding-top:5px; padding-left: 10px;}
.header .menu_box li a:hover h2,
.header .menu_box li a:hover, .header .menu_box li a#current {color:#222;text-shadow: #E6F1FF 0px 0px 3px; background-image:url('../../images/underline.png');
					    background-repeat:repeat-x;
					    background-position:bottom}
.header .menu_box li ul li { float: none; text-align: left; margin:5px 0}
.header .menu_box li ul li a { color: #666; font-size: 12px; margin: 5px;}
.header .menu_box li ul li ul {margin: 4px 2px 4px 10px; padding:0;}
.header .menu_box li ul li ul li {margin:2px; padding:0;}
				
/*Header glow/icons*/
#cp-menu-home {background-image:url('../../images/blue.icon.png');
	background-repeat:no-repeat;
	background-position:0 0}
#cp-menu-home:hover {text-shadow: #8ed1ff 0px 0px 18px;}
#cp-menu-community {background-image:url('../../images/green.icon.png');
	background-repeat:no-repeat;
	background-position:0 0}
#cp-menu-community:hover {text-shadow: #acff08 0px 0px 18px;}
#cp-menu-workspaces {background-image:url('../../images/orange.icon.png');
	background-repeat:no-repeat;
	background-position:0 0}
#cp-menu-workspaces:hover {text-shadow: #ffae00 0px 0px 18px;}
#cp-menu-applications {background-image:url('../../images/red.icon.png');
	background-repeat:no-repeat;
	background-position:0 0}
#cp-menu-applications:hover {text-shadow: #ff96af 0px 0px 18px;}
#cp-menu-developerplatform {background-image:url('../../images/gray.icon.png');
	background-repeat:no-repeat;
	background-position:0 0}
#cp-menu-developerplatform:hover {text-shadow: #aaa 0px 0px 18px;}
#cp-menu-support {background-image:url('../../images/purple.icon.png');
	background-repeat:no-repeat;
	background-position:0 0}
#cp-menu-support:hover {text-shadow: #e285ff 0px 0px 18px;}				

.content { background:url('../../images/bg-content.png') repeat-y 0px 0px;
	   clear: left;
	   padding: 40px 0px;
	   float: left;position:relative;
	   width: 920px;
	   left: 50%;
	   margin-left: -460px;}
.content .teaser { height: 300px; width: 830px; display: block; margin: auto;  }
.content .teaser_hide { display: none;  }

.content a { color: #0099ff; text-decoration:none; border-bottom: 1px dotted;}
.content a:hover {color: #7db63a;}

a.cp-doNotDisplay { display: none; }

#main { padding:10px 60px 30px;text-shadow: #fff 0px 0px 3px;}

/*Breadcrumb*/

#main .toolbox ul{
	list-style: none;
	text-align: left;
	font-size: 1em;
	padding: 11px 5px 11px;
	background: url('../../images/border-breadcrumb.png') no-repeat center center;
}

#main .toolbox ul a{
	color: #878787;
	border-bottom: none;
}

#main .toolbox ul a:hover{
	border-bottom: 1px dotted;
}
#main .toolbox ul li:hover {
	list-style-image: none;
}

/* skinny language selector in the toolbox line */
#main .toolbox #location form {
	float:right; 
	border:none; 
	border-radius:none; 
	box-shadow:none; 
	padding:8px 5px; 
	margin:0;
}

/*H1 is defined at HEADER styles */
/*#main h1 { color: #446888;font-size:22pt; padding:30px 0px 20px 0px;}*/
#main h1, #main h2, #main h3, #main h4, #main h5, #main h6 {font-weight:normal}
#main h2{margin:20px 0px 0px -10px;
		color: #5C8ABB; 
		background: url('../../../images/h3.png') no-repeat left center; 
		padding: 15px 0px 15px 46px; font-family: Georgia; 
		font-size: 1.9em;font-style: italic; letter-spacing: -0.01em;}
#main h3{ color: #0a71b4; padding: 10px 15px 5px 9px; font-size: 1.45em; text-shadow: 0 1px 0 #E4E9EF, 0 2px 1px rgba(162, 187, 213, 0.6);}
#main h4 { color: #557899; font-size: 1.3em; margin-bottom: 5px; padding: 15px 10px 5px 9px; }/*446888*/
#main h5 { color: #0A71B4;font-size:1em; padding:10px 9px;font-weight:bold; text-transform: uppercase;}
#main h6 { color: #A2BBD5;font-size:1.05em; padding:10px 9px;font-weight:bold;}
#main p, #main ul { color: #444; padding: 15px}
#main ul { list-style: circle outside; }
#main ul li ul { padding: 7px 0 7px 20px; }



#main h3 a{
	border: none;
	color: #0a71b4;
}
#main h3 a:hover{
	color: #0a71b4;
}

/*#main {font-size:10pt; line-height:120%;text-shadow: #fff 0px 0px 3px;}*/


#main .app-screenshot a{
	border: none;
}

#main .app-screenshot img{
	margin: 0px auto;
	display: block;
	max-width: 500px;
}

#main p { padding: 10px; line-height:1.7em;}

#main .intro{
	padding: 25px 0px 45px;
	margin:15px 5px 85px;
	background: url("../../../images/bg-intro.png") no-repeat scroll right 0 transparent;
}

#main .intro p{
	font-size: 1.15em;
	padding-left: 10px;
}
#main .intro + p{
	clear: right;
}

#main .intro .buttons{
	margin-left: 16px;
	margin-top: 15px;
}
#main .intro a, #quicklinks a{
	display: inline-block;
	background: #5c8abc;
	color: #fff;
	text-shadow: 1px 1px 0px #054b98;
	padding: 7px 12px;
	margin-right: 10px;
	border-bottom: none;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	-khml-border-radius: 4px;
	border-radius: 4px;
}
#main .intro a:hover, #quicklinks a:hover{
	background: #054b98;
	text-shadow: 1px 1px 0px #5c8abc;
	text-decoration:none;
}

#main img {margin:0px 10px; padding:5px; background:#f1f1f1; border:1px solid #dadada; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;-khtml-border-radius: 3px;}

#main h3 > img, #main .app-icon img, #main .h2-img {
	margin: 0px; background: none; border: none; width: auto;
}

 #main .h2-img {
 	margin-top: 27px;
 	float: left;
 }

#main .app-icon img {
	width: 35px;
}
#main .teaser img{
	border: none; margin: 0px; padding: 0px; background: none;
}
#main .intro h3{
	text-indent: -99999999px;
	background: url('../../../images/welcome.png') no-repeat left top;
}
#main .intro img{
	border: none; margin: -25px 0px 0 0; padding: 0px; background: none;float: right;
}

#main dl,#main ol,#main ul {padding:10px 20px;}

#main ol,#main ul {padding-left:30px;}
#main ol {list-style:decimal}
#main ul {list-style-image:url('../../../images/list-circle.png');}
#main ul.features {list-style-image:url('../../../images/list-feature.png');}

#main form {margin:10px;padding:5px; border:1px solid #E1EAF2; box-shadow: 0px 2px 3px #888; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;-khtml-border-radius: 5px;}
#main fieldset {}
#main legend {}
#main input {}
#main textarea {}

#main table {  color:#444; 
			border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;-khtml-border-radius: 3px;
			margin: 20px auto; min-width: 70%;}
#main table th {color:#FFF;background-color:#A2BBD5; padding:5px;text-align:center; 
			vertical-align: middle;text-shadow: none; padding: 5px 10px; border: 1px dotted;}

#main table th:last-child{ border-right: none;}
#main table tr:nth-child(odd) {background: #e3e7ec;)}
#main table tr:nth-child(even) {}

#main table tr:nth-child(odd) td{
	border-color: #fff;
}
#main table td {padding:5px 10px;}

#main table img{
	border:none;
	background: none;
}

#main table.donations-and-sponsors td { vertical-align:middle; }

#main sup {font-size:7pt;}
#main sub {font-size:7pt;}
#main cite {}
#main pre { font-size:9pt; font-style:"monospace"; color:#555; padding: 2px 5px 2px 20px; border:1px solid #DDD; margin-left:20px;}


/* Download button */
#main a.downloadButton {
    display:block; width: 130px; height:61px; background: url('/images/buttons/download_empty.png'); text-align:center; padding: 40px 10px 15px 130px; font-size:12pt; line-height:20px;
    margin-left: auto;
    margin-right:auto; vertical-align:middle;
    color: #444;
}
#main a.downloadButton:hover {
    text-decoration: none;
}

.table-wrapper {
    padding: 15px;
}

/* Application pages */
#sidebar {
	float: right;
	background: #fdfdfd url('../../../images/bg-sidebar.png') repeat center center;
	width: 200px;
	-moz-box-shadow: 0px 0px 1px rgba(157,157,157,.7);    
    -webkit-box-shadow: 0px 0px 1px rgba(157,157,157,.7);     
    -khtml-box-shadow: 0px 0px 1px rgba(157,157,157,.7); 
	box-shadow: 0px 0px 1px rgba(157,157,157,.7);
	margin: 10px -10px 0px 30px;
	position: relative;
	padding: 10px 0 10px 10px;
}

#sidebar div{
	padding: 15px 0px 0px 40px;
	position:relative;
	background: url('../../../images/bg-infobox.png') no-repeat center center;
	font-size: .85em;
    padding: 15px 0px 0px 40px;
}

#sidebar div p{
	padding: 0px 0 0px 15px;
	background: url(../../../images/narrows.png) no-repeat -2px 1px;
}

#sidebar p:nth-child(2n+2){
	background-position: -2px -18px;
	}
#sidebar div:first-child{
	background: url('../../../images/sidebar-spot-tp.png') no-repeat left top;
	padding-top: 25px;
}


#sidebar div:first-child a{
	margin-left: 20px;
}
#sidebar div:last-child{
	background: url('../../../images/sidebar-spot-bt.png') no-repeat right bottom;
	padding-bottom: 15px;
}
#sidebar div strong{
	font-family: Georgia;
	font-style: italic;
	color: #5c8abb;
	font-size: 1.3em;
	display: block;
	background: url(../../../images/sidebar-icons.png) no-repeat left top;
	padding: 1px 0px 8px 24px;
	margin-left: -10px;	
}


#sidebar div strong[class=categories]{
	background-position: -4px -7px;
}
#sidebar div strong[class=more]{
	background-position: -4px -183px;
}
#sidebar div strong[class=help]{
	background-position: -4px -67px;
}

#sidebar div strong[class=contact]{
	background-position: -4px -126px;
}
#sidebar div strong[class=info]{
	background-position: -4px -239px;
}

#sidebar:before{
	content: ' ';
	background: url(../../../images/sidebar-shadow1.png) no-repeat left bottom;
	width: 13px;
	height: 427px;
	display: block;
	position: absolute;
	bottom: -11px;
	right: -11px;
}

#sidebar:after{
	content: ' ' ;
	background: url(../../../images/sidebar-shadow2.png) no-repeat left bottom;
	position: absolute;
	width: 248px;
	height: 11px;
	bottom: -11px;
	right: 0px;
}
#sidebar #infobox-return:before{
	content: ' ';
	background: url('../../../images/stick-inv.png') no-repeat center center;
	width: 32px;
	height: 32px;
	display: block;
	position: absolute;
	top: -12px;
	right: -11px;
}

#sidebar .infobox:last-child:after{
	content: ' ';
	background: url('../../../images/stick-inv.png') no-repeat center center;
	width: 32px;
	height: 32px;
	display: block;
	position: absolute;
	bottom: -12px;
	left: -15px;
}

#sidebar strong + br{
	line-height: .7em;
}

#sidebar a {
    text-decoration: none;
    border-bottom: none;
    color: #0a71b4;
    line-height: 1.3em;
}

#sidebar a:hover {
   color: #7DB63A;
   border-bottom: 1px dotted;
}

#sidebar strong[class="categories"] + a{
	margin-left: 20px;
}

.app-icon {
    float: left;
    margin: 23px 0 10px;
    padding-left: 13px;
}

#main .app-icon + h2{
    padding-left: 70px;
    background: none;
}

.app-screenshot {
    clear: left;
    margin: 25px 0px 35px;
    width: 7p%;
}
#main .app-screenshot + p {
	clear: both;
	margin-top: 15px;
	padding-top: 20px;
}
.infobox {
    padding-top: 15px;
}


#infobox-return img, .header-image {
    padding: 0px !important;
    border: 0px !important;
    margin: 0px 5px 0px -20px !important;
    background-image: none !important;
    width: 8%;
    vertical-align: middle;
}

.app-category, .international-site {
    float: left;
    display: table-cell;
    max-width: 240px;
    width: 30%;
    margin: 10px 1.5% 15px 1.5%;
    text-align: left;
    border: 1px solid #dadada;
    -moz-box-shadow: 0px 1px 1px rgba(157,157,157,.4);
    box-shadow: 0px 1px 1px rgba(157,157,157,.4);
    -webkit-box-shadow: 0px 1px 1px rgba(157,157,157,.4);
    -khtml-box-shadow: 0px 1px 1px rgba(157,157,157,.4);
    background: #ebebeb url('../../../images/bg-category.png') repeat-x left bottom;
    font-size: .87em;
    color: #888 !important;
    line-height: 1.2em !important;
    padding: 0px !important;
    min-height: 97px;
}

.app-category:hover{
	 background: #ebebeb url('../../../images/bg-category-over.png') repeat-x left bottom;
}

.app-category:hover a span{
	background: url('../../../images/bg-app-a.png') repeat-x left center;
}

.app-category > span{
	display: block;
	margin-top: 44px;
	position: absolute;
	margin-left: 113px;
	width: 115px;
	z-index: 1;
}

/*.app-category img, .international-site img {
    float: left;
    display: block;
    margin: 0px 10px 0px 0px;
    background: none !important;
}*/

.app-category a:before {
	content: ' ';
	background: url('../../../images/stick.png') no-repeat center center;
	width: 32px;
	height: 32px;
	display: block;
	position: absolute;
	top: 4px;
	left: 2px;
}

.app-category a:after {
	content: ' ';
	background: url('../../../images/stick.png') no-repeat center center;
	width: 32px;
	height: 32px;
	display: block;
	position: absolute;
	bottom: 0px;
	left: 74px;
	
}
.app-category a, .international-site a {
   font-weight: bold;
    text-decoration: none;
    display: block;
   border:none;
	width: 90%;
	font-size: 1.2em;
	position: relative;
	float: left;
	padding: 17px 12px 14px 15px;
	z-index: 2;
}


#main .app-category a img{
	background: #fdfdfd url('../../../images/bg-category-box.png') no-repeat center center;
	-moz-border-radius: 4px:
    -webkit-border-radius: 4px;
    -khtml-border-radius: 4px;
    border-radius: 4px;
    border: 1px solid #DADADA;
    -moz-box-shadow: 0px 0px 1px rgba(157,157,157,.7);    
    -webkit-box-shadow: 0px 0px 1px rgba(157,157,157,.7);     
    -khtml-box-shadow: 0px 0px 1px rgba(157,157,157,.7); 
	box-shadow: 0px 0px 1px rgba(157,157,157,.7); 
	padding: 15px;
	float: left;
	margin: 0px;
}

#main .app-category br{
	display: none;
}

#main .app-category a span{
	position: absolute;
	margin-left: 23px;
	margin-top: 3px;
	color: #0A71B4;
	display: block;
	padding: 5px 10px;
	left: 82px;
}


.international-site {
    height: auto !important;
}

/*
   OCS Stuff
*/

#rss, #required-area, .info-area{
	float: left;
	width: 30.5%;
	margin-left: 1%;
	margin-right: 1.8%;
	margin-top: 45px;
}

#rss h3, #required-area h3, #main .info-area h4{
	background: url('../../../images/icons.png') no-repeat left top;
	padding: 9px 0 5px 36px;
	font-size: 1.25em;
}
#required-area h3{
	background-position: left -45px;
}

/*News style at homepage */

#main .info-area h4{
	background-position: left -90px;
	color: #0A71B4;
	border:none;
}

#main .info-area ul, #main .info-area ul li:hover{
	list-style: none;
	padding: 0px;
}

#main .info-area ul li{
	margin: 0px 0px 0px 15px;
}

#main .info-area ul a{
	color: #777;
	display: block;
	margin: 9px 0px 0px 0px;
	line-height: 1.4em;
	border-bottom: 0px;
}

.title{
	display: block;
	color:#0099FF;
	font-family: Georgia, serif;
	font-style: italic;
	font-size: .9em;
	font-weight: bold;
}
.title:hover{
	color: #7DB63A;
	}
.date{
	text-shadow: none;
	color: #039aff;
	padding: 2px 0px;
	font-size: .85em;
}

.desc{
	background: url(../../../images/list-news.png) no-repeat -4px 3px;
	padding-left: 13px;
}

#main .info-area p{
	text-align: right;
}

/******/

#main table.ocs img {
    padding: 1px !important;
    margin: 0px;
}

#main table.ocs {
    border:none !important;
}

.ocs-hackergotchi img {
    height: 32px !important;
    width: 32px !important;
}

.ocs-applicationshot {
    padding: 10px 0px 10px 10px !important;
}

.ocs-latestapp {
    padding: 10px 10px 10px 10px !important;
}

.ocs-content, .ocs-linksbar, .ocs-latestapp {
    vertical-align: middle;
    text-align: left;
}

.ocs-title {
    font-weight: bold;
    padding-right: 10px;
}

.ocs-linksbar {
    text-align: right !important;
}

.ocs-linksbar img {
    margin: 0px 2px 2px 2px !important;
}

.screenshot {
    margin-top: 20px; margin-bottom: 20px;
}

.toggle {float: right; width: auto;padding: 10px 10px 0 10px;color: #888; text-shadow: #fff 0px 0px 3px; text-decoration:none; font-size: 12px; font-weight:normal; display none; }

#hotspot {float: right;}

.up{
	position: absolute;
	bottom: -31px;
	right: -37px;
}
.up a{
	text-indent: -99999999px;
	background: url('../../../images/up.png') no-repeat left top;
	display: block;
	width: 121px;
	height: 128px;
	border-bottom: none;
}
.up a:hover{
	background-position: left -135px;
	
}

.two_left{
	width: 40%;
	float: left;
}

.two_left{
	width: 40%;
	float: left;
}

.c-left{
	clear: left;
}

/*Position of navigation category depends on monitor resolution */

@media screen and (min-width: 600px) and (max-width: 1280px){
	.category-nav{
	top: 280px;
	position: fixed;
	right: 168px;
	}
} 
@media screen and (min-width: 1281px){
	.category-nav{
	top: 115px;
	position: absolute;
	right: -5px;
	}
}
.category-nav{
	width: 43px; 
	float: right;
	background: url('../../../images/category.png') no-repeat left -2px;
	height: 32px;
}


.category-nav:hover{
	background: url('../../../images/category.png') no-repeat left -41px;
}

.category-nav:hover a{
	position: absolute;
	display: block;
	width: 200px;
	left: -200px;
	padding-right: 42px;
	border-bottom: none;
	padding-bottom: 34px;
	background: url('../../../images/back-category.png') no-repeat 165px bottom;
	height: 22px;
	top: -2px;
	z-index: 1000;
}

.category-nav a{
	display:none;
}

.category-nav a span{
	display: block;
	background: #fdfdfd;
	padding: 10px 5px;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	-khtml-border-radius: 3px;
	-moz-box-shadow: 0px 1px 4px #ccc;
	-webkit-box-shadow: 0px 1px 4px #ccc;
	-khtml-box-shadow: 0px 1px 4px #ccc;
	box-shadow: 0px 1px 4px #ccc;
	text-align: center;
}
.main-content .app-screenshot + p:first-child{
	clear:both;
}
.app-screenshot + p strong:first-child{
	color: #0A71B4;
	font-size: 1.1em;
}

.app-additional-info-paragraph-hidden{
	background: url(../../../images/sidebar-icons.png) no-repeat -4px -170px;
	padding-left: 22px !important;
}

.app-additional-info-paragraph-shown{
	background: url(../../../images/sidebar-icons.png) no-repeat -4px -286px;
	padding-left: 22px !important;
}

#app-additional-info li{
	color: #999;
}

#app-additional-info li span{
	color: #444;
}

/*Apps landing page*/

#module + table div{
	position: relative;
}

#module + table div:before {
	content: ' ';
	background: url('../../../images/stick.png') no-repeat center center;
	width: 32px;
	height: 32px;
	display: block;
	position: absolute;
	top: -35px;
	left: -34px;
}

#module + table div:after {
	content: ' ';
	background: url('../../../images/stick.png') no-repeat center center;
	width: 32px;
	height: 32px;
	display: block;
	position: absolute;
	bottom: -40px;
	right: -385px;
	
}

#module + table tr:nth-child(2n+1){
	background: url("../../../images/bg-sidebar.png") repeat scroll center center #FDFDFD;
	-moz-box-shadow: 0 0 1px rgba(157, 157, 157, 0.7);
	-webkit-box-shadow: 0 0 1px rgba(157, 157, 157, 0.7);
	-khtml-box-shadow: 0 0 1px rgba(157, 157, 157, 0.7);
	box-shadow: 0 0 1px rgba(157, 157, 157, 0.7);
}

#module + table td:first-child{
	background: url("../../../images/sidebar-spot-tp.png") no-repeat scroll left top transparent;
}

#module + table td:last-child{
	background: url("../../../images/sidebar-spot-bt.png") no-repeat scroll right bottom transparent;
}
#module + table td{
	vertical-align: top;
	padding: 25px;
}

#module + table td a[href$=png]{
	border-bottom: none;
}


#quicklinks{
	visibility: hidden;
	margin-bottom: 25px;
}

#quicklinks a{
	visibility: visible;
	margin-right: 0px;
}
/******/


/*Footer style*/

.footer { text-align: left;
	  background-image: url('../../images/page-fr-alt.png');
	  background-repeat: no-repeat;
	  background-position: 0 0;
	  width: 850px; height: 60px; padding: 40px 0 0 0;
	 }
.footer .module { float: left; width: 25%; margin: 0 10px 30px 10px}
.footer .module {font-size:x-small;}

#footer { clear:both; color: #aaa; font-size:.8em; width:100%;background:#505050 url('../../images/footer.png') no-repeat center top;padding:5px 0 5px 0;}
#footer a {color: #eee;}
	
#footer_text{
	margin-top: 45px;
	margin-bottom: 20px;
}
