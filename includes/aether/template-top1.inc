<header id="KGlobalHeader mb-2" class="header clearfix d-print-none">
    <nav id="kHeaderNav" class="navbar navbar-expand-md container" >
    <a class="navbar-brand active kde-logo" href="/" id="KGlobalLogo">
        <?= $site_title ?>
    </a>
    <?php if ((isset($menuright) && count($menuright) > 0)) { ?>
    <div class="collapse navbar-collapse justify-content-between" id="kde-navbar">
        <ul class="navbar-nav">
            <?php foreach ($menuright as $url=>$link) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= "$site_root/$url" ?>">
                        <?= $link ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <button class="navbar-toggler navbar-toggler-right webfonts-changed" type="button" data-toggle="collapse" data-target="#kde-navbar" aria-controls="kde-navbar" aria-expanded="false" aria-label="Toggle navigation" data-_extension-text-contrast="fg">
        <span class="navbar-toggler-icon"></span>
    </button>
    <?php } ?>
</header>
<main class="container mt-4">
    <div class="row">
        <div class="col-12 col-lg-9">
