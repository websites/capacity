<?php // This file contains content for the html header
?>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <link rel="meta" href="http://www.kde.org/labels.rdf" type="application/rdf+xml" title="ICRA labels" />
  <meta name="trademark" content="KDE e.V." />
  <meta name="description" content="K Desktop Environment Homepage, KDE.org" />
  <meta name="MSSmartTagsPreventParsing" content="true" />
  <meta name="robots" content="all" />
  <meta name="no-email-collection" content="http://www.unspam.com/noemailcollection" />
<?php
  if (file_exists("$site_root/favicon.ico")) {
    print "<link rel=\"shortcut icon\" href=\"$site_root/favicon.ico\" />";
    print "<link rel=\"icon\" href=\"$site_root/favicon.ico\" />";
  } else {
    print "<link rel=\"icon\" href=\"/media/images/favicon.ico\" />";
    print "<link rel=\"shortcut icon\" href=\"/media/images/favicon.ico\" />";
  }

  if (isset($rss_feed_link))
    print "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"$rss_feed_title\" href=\"$rss_feed_link\" />\n";
  if (isset($rss_feed2_link))
    print "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"$rss_feed2_title\" href=\"$rss_feed2_link\" />\n";

  $cssparameters = "";

  if (file_exists ("$document_root/css.inc")) {
    # Possibilities: doc_root is what the server uses, too, so this site
    # is in the root of a (v)host. Fine. Otherwise, maybe we're in a
    # subdir and need to include our css.inc from that subdir starting
    # from the server's doc root (since css.php uses the server's doc root).
    if ( $document_root == $_SERVER ['DOCUMENT_ROOT'] )
      $inc = "/";
    else if ( $_SERVER ['DOCUMENT_ROOT'] == substr($document_root, 0, strlen( $_SERVER ['DOCUMENT_ROOT'] )) )
      $inc = substr($document_root,strlen( $_SERVER ['DOCUMENT_ROOT'] ));
    else $inc = "/";
    $cssparameters .= "&amp;css-inc=" . $inc;
  }
?>

  <link rel="stylesheet" media="screen" type="text/css" title="KDE Colors" href="/media/css.php?mode=normal<?php echo $cssparameters?>" />
  <link rel="stylesheet" media="print, embossed" type="text/css" href="/media/css.php?mode=print<?php echo $cssparameters?>" />
  <link rel="alternate stylesheet" media="screen, aural, handheld, tty, braille" type="text/css" title="Flat" href="/media/css.php?mode=flat<?php echo $cssparameters?>" />
  <link rel="alternate stylesheet" media="screen" type="text/css" title="Browser Colors" href="/media/css.php?color&amp;background&amp;link" />
  <link rel="alternate stylesheet" media="screen" type="text/css" title="White on Black" href="/media/css.php?color=%23FFFFFF&amp;background=%23000000&amp;link=%23FF8080" />
  <link rel="alternate stylesheet" media="screen" type="text/css" title="Yellow on Blue" href="/media/css.php?color=%23EEEE80&amp;background=%23000060&amp;link=%23FFAA80" />
