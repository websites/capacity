<?php // This file contains content between the menu and the end of html body

  // figure out the contact
  if (isset($name) && isset($mail))
    $contact = i18n_var("Maintained by") . " <a href=\"mailto:$mail\">$name</a><br />\n";
  else
    $contact = i18n_var("Maintained by <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">The KDE Webmaster</a><br />\n");
  if (!isset($legalLink))
    $legalLink = "http://www.kde.org/contact/impressum.php";
?>

             <div class="menutitle"><div>
               <h2 id="cp-menu-search"><?php i18n("Search:")?></h2>
             </div></div>

             <div></div>
<?php
if (isset($_GET["newsearch"])) {
?>
            <form action="http://www.kde.org/search/" id="cse-search-box">
            <p style="text-align:center">
                <input type="hidden" name="newsearch" value="true" />
                <input type="hidden" name="cx" value="009530675943227777520:cmvt6dt_jkm" />
                <input type="hidden" name="cof" value="FORID:11" />
                <input type="hidden" name="ie" value="UTF-8" />
                <span class="cp-doNotDisplay"><label for="Search" accesskey="4"><?php i18n("Search:")?></label></span>
                <input style="width:80%" type="text" name="q" size="31" id="Search" onblur="defaultStyle(this)" onfocus="clearedStyle(this)" />
                <input style="width:80%" type="submit" name="sa" value=" <?php i18n("Search")?> " id="searchButton" />
            </p>
            </form>
            <script type="text/javascript">
                function defaultStyle(e) {e.style.background = '#FFFFFF url(/media/images/google_custom_search_watermark.gif) left no-repeat';}
                function clearedStyle(e) {e.style.background = '#FFFFFF';}
                if (!/[&?]q=[^&]/.test(location.search)) defaultStyle(document.getElementById('Search')); // do not show background image if there is already a text entered
            </script>
<?php
if ($page_title == "KDE.org Search") // we just need this file if we display search results
    echo "<script type=\"text/javascript\" src=\"http://www.google.com/afsonline/show_afs_search.js\"></script>";
?>
<?php
} else {
?>
             <form method="get" name="Searchform" action="/media/search.php"><p style="text-align:center">
               <span class="cp-doNotDisplay"><label for="Search" accesskey="4"><?php i18n("Search:")?></label></span>
               <input style="width:80%" type="text" size="10" name="q" id="Search" value="" /><br />
               <input style="width:80%" type="submit" value=" <?php i18n("Search")?> " name="Button" id="searchButton" />
             </p></form>
<?php
}
?>

<?php
    if (isset($site_languages) && count($site_languages) > 1)
    {
?>    
             <div class="menutitle"><div>
               <h2 id="cp-menu-language"><?php i18n("Language:")?></h2>
             </div></div>
             
             <div><div>
             <p>
             <form method="get" name="languagecombo" action="">
                <select style="width:80%" name="site_locale">
                    <?php
                        foreach ($site_languages as &$value) {
                            if ($value == $site_locale) $selected=" selected='selected'";
                            else $selected="";
                            print "<option value='".$value."'".$selected.">".languageName($value)."</option>";
                        }
                    ?>
                </select>
                <input style="width:80%" type="submit" value=" <?php i18n("Change language")?> " id="languageButton" />
             </form>
             </p>
             </div></div>
<?php
}
?>

<?php
    if (isset($site_showkdeevdonatebutton) && $site_showkdeevdonatebutton && (!isset($page_disablekdeevdonatebutton) || !$page_disablekdeevdonatebutton))
    {
?>
            <div class="menutitle">
                <div><h2><?php i18n("Donate:");?> <a href='//www.kde.org/community/donations/index.php#money'><small style="float: right; margin-right: 1em;"><?php i18n("(Why?)");?></small></a></h2></div>
            </div>
            <p>
            <form action="https://www.paypal.com/en_US/cgi-bin/webscr" method="post" onsubmit="return amount.value >= 2 || window.confirm('<?php print i18n_var("Your donation is smaller than %1€. This means that most of your donation\\nwill end up in processing fees. Do you want to continue?", 2);?>');">
                <input type="hidden" name="cmd" value="_donations" />
                <input type="hidden" name="lc" value="GB" />
                <input type="hidden" name="item_name" value="Development and communication of KDE software" />
                <input type="hidden" name="custom" value="<?php print htmlspecialchars("//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]/donation_box"); ?>" />
                <input type="hidden" name="currency_code" value="EUR" />
                <input type="hidden" name="cbt" value="Return to www.kde.org" />
                <input type="hidden" name="return" value="https://www.kde.org/community/donations/thanks_paypal.php" />
                <input type="hidden" name="notify_url" value="https://www.kde.org/community/donations/notify.php" />
                <input type="hidden" name="business" value="kde-ev-paypal@kde.org" />
                <input type='text' name="amount" value="10.00" style='text-align: right; padding-right: 1em; border: 1px solid #CCC; width: 60px; height: 23px; ' /> €
                <button style='cursor: pointer; background-color: #0070BB; border: 1px solid #0060AB; color: #FFF; height: 27px;' type='submit'><?php i18n("Donate");?></button>
            </form>
            </p>
<?php
}
?>

          </div>
        </div>
        <div class="clearer"></div>
      </div>
      <div class="clearer"></div>
    </div>
    <div id="end_body"></div>

    <div id="footer"><div id="footer_text">
        <?php print $contact; ?>
				<?php i18n("KDE<sup>&#174;</sup> and <a href=\"/media/images/trademark_kde_gear_black_logo.png\">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of ")?>
				<a href="http://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> |
        <a href="<?php print $legalLink; ?>"><?php i18n("Legal")?></a>
    </div></div>
  </div>

<!--
WARNING: DO NOT SEND MAIL TO THE FOLLOWING EMAIL ADDRESS! YOU WILL
BE BLOCKED INSTANTLY AND PERMANENTLY!
<?php
  $trapmail = "aaaatrap-";
  $t = pack('N', time());
  for($i=0;$i<=3;$i++) {
      $trapmail.=sprintf("%02x",ord(substr($t,$i,1)));
  }
  $ip=$_SERVER["REMOTE_ADDR"];
  sscanf($ip,"%d.%d.%d.%d",$ip1,$ip2,$ip3,$ip4);
  $trapmail.=sprintf("%02x%02x%02x%02x",$ip1,$ip2,$ip3,$ip4);

  echo "<a href=\"mailto:$trapmail@kde.org\">Block me</a>\n";
?>
WARNING END
-->

<?php
  if (isset ($_GET ['javascript'])) {
?>

<script type="text/javascript">
function fitFooter() {
  try {
    document.getElementById ('body').style.minHeight = (window.innerHeight - document.getElementsByTagName ('body')[0].offsetHeight + document.getElementById ('body').offsetHeight) + "px";
    window.setTimeout ("fitFooter()", 200);
  } catch (e) {
  }
};
window.setTimeout ("fitFooter()", 100);
</script>

<?php
}
?>
