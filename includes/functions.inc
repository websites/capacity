<?php
/*
 * Filename: 	functions.php
 * Description: supplies functions for the KDE Homepage.
 * Functions: 	printNews(items)
 * Author: 	Sebastian Faubel
 * Modified by: Jason Bainbridge and Chris Howells
 * Comments: Added some caching code for the rdf generation.
 * NB. Those functions should be merged together at sometime
 *     for the sake of cleaner code
 *	   + the html needs to be cleaned up
 */

include_once("classes/class_rdf.inc");
include_once("classes/class_faq.inc");
include_once("classes/class_image_gallery.inc");
include_once("classes/class_appinfo.inc");
include_once("classes/class_hotspot.inc");

function setup_site ()
{
  global $site_root;
  global $document_root;
  global $url_root;
  global $current_relativeurl;

  // start with empty site_root...
  $site_root = "";

  // start with values queried from php...
  $document_root = getcwd();
  $url_root = dirname($_SERVER['PHP_SELF']);

  // search for site.inc, add hard limit here, just to be 1000% sure this will finish...
  // each site must have a site.inc, really, or be in it's own documentroot, both will work!
  $i = 0;
  while ($i < 32)
  {
    // webroot reached
    if ($url_root == "" || $url_root == "/")
      break;

    // site.inc found, root...
    if (file_exists ("$document_root/site.inc"))
      break;

    $document_root = dirname ($document_root);
    $url_root = dirname ($url_root);
    $site_root .= "../";

    $i++;
  }

  // if no site_root, prefix at least with ., else, remove the trailing /
  if ($site_root == "")
    $site_root = ".";
  else
    $site_root = substr ($site_root, 0, strlen($site_root) - 1);

  // no trailing / in url_root
  if ($url_root == "/")
    $url_root = "";

  // calculate the url of the current file relative to $menu_baseurl;
  $current_relativeurl = substr ($_SERVER['PHP_SELF'], strlen($url_root), strlen($_SERVER['PHP_SELF']) - strlen($url_root));
  if ($current_relativeurl[0] == "/")
    $current_relativeurl = substr ($current_relativeurl, 1, strlen($current_relativeurl));
}

function strip_cdata ($string) 
{ 
  preg_match_all("/<!\[cdata\[(.*?)\]\]>/is", $string, $matches); 
  return str_replace($matches[0], $matches[1], $string); 
} 

function kde_general_news ($file, $items, $summaryonly)
{
  global $site_locale;
  startTranslation($site_locale);

  if ($summaryonly)
    print "<h2 id=\"news\">" . i18n_var("Latest News") . "</h2>\n";

  $news = new RDF();
  $rdf_pieces = $news->openRDF($file);

  if(!$items)
  {
     $items = 5; // default
  }
  $rdf_items = count($rdf_pieces);
  if ($rdf_items > $items)
  {
     $rdf_items = $items;
  }

  //only open the file if it has something in it
  if ($rdf_items > 0)
  {
    /* Don't display the last story twice
    * if there is less than the requested number of stories
    * in the RDF file */
    if ($rdf_items < $items)
    {
      $rdf_items = $rdf_items - 1;
    }

   $alternate = false;

    print "<table class=\"table_box\" cellpadding=\"6\">\n";

    if ($summaryonly)
      print "<tr><th>" . i18n_var("Date") . "</th>\n<th>" . i18n_var("Headline") . "</th></tr>\n";

    $prevDate = "";
    for($x=1;$x<=$rdf_items;$x++)
    {
      $alternate = !$alternate;
      if ($alternate)
      {
        $color = "newsbox1";
      }
      else
      {
        $color = "newsbox2";
      }
      preg_match("@<title>(.*)</title>@s", $rdf_pieces[$x], $title);
      preg_match("@<date>(.*)</date>@s", $rdf_pieces[$x], $date);
      preg_match("@<fullstory>(.*)</fullstory>@s", $rdf_pieces[$x], $fullstory);
      print "<tr>\n";

      $printDate = i18n_var($date[1]);
      $printTitle = i18n_var($title[1]);
      if ($summaryonly)
      {
        print "<td valign=\"top\" class=\"$color\">".(($printDate == $prevDate) ? "&nbsp;" : "<b>$printDate</b>")."</td>\n";
        print "<td class=\"$color\"><a href=\"news.php#item" . preg_replace("@[^a-zA-Z0-9]@", "", $title[1]) . "\">$printTitle</a></td>\n";
        $prevDate = $printDate;
      }
      else
      {
        $printFullstory = strip_cdata(i18n_var($fullstory[1]));
        print "<td class=\"newsbox2\"><h3><a name=\"item" . preg_replace("@[^a-zA-Z0-9]@", "", $title[1]) . "\">$printDate: $printTitle</a></h3></td>\n";
        print "</tr><tr><td class=\"newsbox2\">$printFullstory</td>\n";
      }

      print "</tr>\n";

    }
    print "</table>\n";
  }
}

function begin_under_construction()
{
  if ($_SERVER["QUERY_STRING"] != "test")
    print("<!-- Still Under Construction\n");
}

function end_under_construction()
{
  if ($_SERVER["QUERY_STRING"] != "test");
    print("     Still Under Construction -->\n");
}

function startTranslation($dictionary)
{
  global $document_root;
  global $default_site_locale;

  if (!isset($_SERVER['REQUEST_URI']))
    return;
  if ((($dictionary != "en") || (isset($default_site_locale) && $default_site_locale != "en")) && basename($dictionary) === $dictionary)
  {
    global $text; //needed!

    $media_file = "i18n/" . $dictionary . "/" . "capacity.inc";
    @include($media_file);

    $topleveldir = explode("/", $_SERVER['REQUEST_URI']);
    if ($topleveldir[1])
    {
      $dir_file = "i18n/" . $dictionary . "/" . $topleveldir[1] . ".inc";
      @include($dir_file);
    }

    global $translation_file;
    if (isset($translation_file))
    {
      $dir_file = $document_root . "/i18n/" . $dictionary . "/" . $translation_file . ".inc";
      @include($dir_file);
    }
  }
}

function i18nDebug()
{
	global $text;
	print "text contains " . count($text) . " items \n";
}

function i18n_noop()
{
	if (func_num_args() < 1) return "";
	return func_get_arg(0);
}

function i18n_var()
{
	if (func_num_args() < 1) return "";
	
	global $text;
	global $default_site_locale;
	global $site_locale;
	global $enable_xtest;
	
	$translate = func_get_arg(0);
	
	if ($site_locale == "en" && (!isset($default_site_locale) || $default_site_locale == "en"))
	{
		$result = $translate;
	}
	else
	{
		if ($enable_xtest and $site_locale == "x-test")
		{
			$result = "x".$translate."x";
		}
		else
		{
			if (isset($text) && array_key_exists($translate, $text))
			{
				$result = $text[$translate];
			}
			else
			{
				$result = $translate;
			}
		}
	}
	
	for ($i = 1; $i < func_num_args(); $i++) {
		$argument = func_get_arg($i);
		$result = str_replace("%".$i, $argument, $result);
	}
	return $result;
}

function i18n($translate)
{
	global $text;
	global $default_site_locale;
	global $site_locale;
	global $enable_xtest;
	if ($site_locale == "en" && (!isset($default_site_locale) || $default_site_locale == "en"))
	{
		print $translate;
	}
	else
	{
		if ($enable_xtest and $site_locale == "x-test")
		{
			print "x".$translate."x";
		}
		else
		{
			if (isset($text) && array_key_exists($translate, $text))
			{
				print $text[$translate];
			}
			else
			{
				print $translate;
			}
		}
	}
}

function languageName($code)
{
	switch ($code)
	{
		case "ca": return "Català";
		case "de": return "Deutsch";
		case "gl": return "Galego";
		case "el": return "Ελληνικά";
		case "en": return "English";
		case "es": return "Español";
		case "et": return "Eesti";
		case "fi": return "Suomi";
		case "fr": return "Français";
		case "it": return "Italiano";
		case "ko": return "한국어";
		case "nl": return "Nederlands";
		case "pl": return "Polski";
		case "pt": return "Português";
		case "pt_BR": return "Português brasileiro";
		case "ru": return "Русский";
		case "sv": return "Svenska";
		case "tr": return "Türkçe";
		case "uk": return "Українська";
	}
	return "Unknown";
}

function niceFileSize($path)
{
	if (file_exists($path))
	{
		$size = filesize($path);
		if ($size > (1024 * 1024))
		{
			echo "(" . round($size/(1024*1024)) . "MB)";
		}
		else
		if ($size > 1024)
		{
			echo "(" . round($size/1024) . "KB)";
		}
		else
		{
			echo "(" . round($size) . "B)";
		}
	}
	else
	{
		echo "(File not available on this server, use <a href=\"http://printing.kde.org\">printing.kde.org</a> or <a href=\"http://www.koffice.org\">koffice.org</a> instead)";
	}
}

function siteLogo($path, $width, $height)
{
	return "src=\"$path\" width=\"$width\" height=\"$height\"";
}

function screenshot($thumbnail, $filename, $align=false, $description=false, $height=false) {
    if (!$align) {
        $align="center";
    }

    $out = '<div  align="'. $align .'" class="screenshot">';
    if (!empty($filename)) {
            //$out .= '<a href="screenshots/'. str_replace(".png", ".jpg", $filename) .'" title="Click to enlarge">';
            $out .= '<a href="screenshots/'. $filename .'" title="Click to enlarge">';
    }
    if ($height) {
        $height=" height=\"$height\" ";
    }
    //$out .= '<img src="screenshots/' . str_replace(".png", ".jpg", $thumbnail) .'" align="'. $align .'" ' . $height . ' />';

    //Set alt and title texts in the image
    if (!empty($description)) {
        $altTitleText = $description;
    } else {
        $altTitleText = $thumbnail;
    }

    $out .= '<img src="screenshots/' . $thumbnail .'" style="text-align:'. $align .'" ' . $height . 'alt="' . $altTitleText . '" title="' . $altTitleText . '"/>';
    if (!empty($filename)) {
            $out .= '</a>';
    }
    if ($description) {
        $out .= '<br /><em>' . $description . '</em>';
    }
    $out .= '</div>';
    print $out;
};

//Convenience function to print screenshots
//It uses "thumbs/$filename" as a thumb, and "center" alignment
function centerThumbScreenshot($filename, $description) {
    screenshot("thumbs/".$filename, $filename, false, $description);
}
?>
