<?php // This file contains content between the top of the html body and the heading
?>
<script type="text/javascript" src="/media/javascripts/jquery-1.4.2.min.js"></script>
<div id="container">
<div id="header">
  <div id="header_top">
    <?php if (file_exists($document_root.'/site.png')) { ?>
      <img alt ="" src="<?php echo $url_root.'/site.png';?>" style="float:right;margin-right:10px;" />
    <?php } ?>
    <div><div>
    <img alt ="" width=35px src="/media/images/kde-white.svg"/>
    <?php echo $site_title;?>
    
  </div></div></div>
  <div id="header_bottom">
<?php if (! isset ($hidelocation) || $hidelocation == false) { ?>
    <div id="location">
      <ul>
	<li><?php $kde_menu->showLocation() ?></li>
      </ul>
    </div>
<?php } ?>

    <div id="menu">
<?php
    if (isset ($menuright) && count ($menuright) > 0) {
        echo '<ul>';

        foreach ($menuright as $url=>$link)
            echo '<li><a href="'."$site_root/$url".'">'.$link."</a></li>\n";
        echo '</ul>';
    } else {
        echo '&nbsp;';
    }
?>
    </div>
  </div>
</div>
<!-- End page header -->
    <div id="body_wrapper">
      <div id="body">
        <!-- begin main content -->
        <div id="right">
          <div class="content">
          <div id="main">
            <div class="clearer">&nbsp;</div>
