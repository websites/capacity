<?php 

if (isset ($templatepath))
    include $templatepath."template-bottom1.inc";
else
    include "template-bottom1.inc";

if ($site_menus > 0 && $templatepath != "chihuahua/") {
    if (isset($site) && $site == "developer")
    {
        echo $template_menulist1."\n";
        $kde_menu->currentMenu();
        echo $template_menulist3."\n";

        if ($kde_menu->activeSection () != 0) {
            echo $template_menulist1."\n";
            $kde_menu->currentMenu(true);
            echo $template_menulist3."\n";
        }
    }
    else
        $kde_menu->show();
}

?>
<div class="cp-doNotDisplay">
  <h2><?php i18n ('Global navigation links')?></h2>
  <ul>
    <li><a href="http://www.kde.org/" accesskey="8"><?php i18n("KDE Home")?></a></li>
    <li><a href="http://accessibility.kde.org/" accesskey="9"><?php i18n("KDE Accessibility Home")?></a></li>
    <li><a href="/media/accesskeys.php" accesskey="0"><?php i18n("Description of Access Keys")?></a></li>
    <li><a href="#cp-content"><?php i18n("Back to content")?></a></li>
    <li><a href="#cp-menu"><?php i18n("Back to menu")?></a></li>
  </ul>
</div>

<?php
if (isset ($templatepath))
    include $templatepath."template-bottom2.inc";
else
    include "template-bottom2.inc";

?>

<?php if(isset($piwikEnabled) && $piwikEnabled && isset($piwikSiteID)) { ?>
<!-- Piwik -->
<script type="text/javascript">
  var pkBaseURL = "https://stats.kde.org/";
  document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
  piwik_action_name = '<?php print($page_title); ?>';
  piwik_idsite = <?php print($piwikSiteID); ?>;
  piwik_url = pkBaseURL + "piwik.php";
  piwik_log(piwik_action_name, piwik_idsite, piwik_url);
</script>
<script type="text/javascript" src="/media/javascripts/piwikbanner.js"></script>
<object><noscript><p>Analytics <img src="https://stats.kde.org/piwik.php?idsite=<?php print($piwikSiteID); ?>" style="border:0" alt=""/></p></noscript></object>
<!-- End Piwik Tag -->
<?php } ?>

<?php
if (function_exists('useCustomTracker')) {
  useCustomTracker();
}
?>

</body>
</html>
