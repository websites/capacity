<?php // This file contains content between the menu and the end of html body
?>
      <?php if ($site_menus > 0) { ?>
        <?php if ($site_menus == 2) { ?>
          </td>
          <td valign="top" id="rightmenu" width="25%">
        <?php } ?>
    
        <?php
          $searchstart = '<div class="menu_box"><h2 id="t_search">'. i18n_var("Search") . "</h2>\n<div id=\"search\">\n";
          $hotspotstart = '<div class="menu_box"><h2 id="t_hotspot">' . i18n_var("Hotspot") . "</h2>\n<div id=\"hotspot\">\n";
          $end = "</div></div>\n";
          if (!isset($site_search) || ($site_search === true)) {
              print $searchstart;
              include_once("search.inc");
              print $end;
          }
          else if ($site_search == "custom") {
              print $searchstart;
              include_once("$site_root/includes/search.inc");
              print $end;
          }
    
          if (isset($site_hotspot)) {
            if ($site_hotspot === true) {
                print $hotspotstart;
                include_once("hotspot.inc");
                print $end;
            }
            else
            if ($site_hotspot == "custom") {
                print $hotspotstart;
                include_once("$site_root/includes/hotspot.inc");
                print $end;
            }
          }
        ?>
    
        <?php if ($site_menus == 2) { ?>
          <?php include('menu_right.inc'); ?>
        <?php } ?>
      <?php } ?>
      </td>
    </tr>
    </table>
  </td>
</tr>

<?php
  // figure out the contact
  if (isset($name) && isset($mail))
    $contact = i18n_var("Maintained by") . " <a href=\"mailto:$mail\">$name</a><br />\n";
//  else
//    $contact = i18n_var("Maintained by <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">kde.org Webmaster</a><br />\n");
  if (!isset($legalLink))
    $legalLink = "http://www.kde.org/contact/impressum.php";
?>

<!-- table row holding the footer -->
<tr>
  <td id="footer">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td id="footer_text">
        <?php print $contact; ?>
        <?php i18n('KDE<sup>&#174;</sup> and <a href="/media/images/kde_gear_black.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="http://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a>');?> |
        <a href="<?php print $legalLink; ?>"><?php i18n("Legal")?></a>
      </td>
    </tr>
    </table>
  </td>
</tr>
</table>

<!--
WARNING: DO NOT SEND MAIL TO THE FOLLOWING EMAIL ADDRESS! YOU WILL
BE BLOCKED INSTANTLY AND PERMANENTLY!
<?php
  $trapmail = "aaaatrap-";
  $t = pack('N', time());
  for($i=0;$i<=3;$i++) {
      $trapmail.=sprintf("%02x",ord(substr($t,$i,1)));
  }
  $ip=$_SERVER["REMOTE_ADDR"];
  sscanf($ip,"%d.%d.%d.%d",$ip1,$ip2,$ip3,$ip4);
  $trapmail.=sprintf("%02x%02x%02x%02x",$ip1,$ip2,$ip3,$ip4);

  echo "<a href=\"mailto:$trapmail@kde.org\">Block me</a>\n";
?>
WARNING END
-->
