<?php

function kde_parse_date ($date)
{
   $parsedDate = strtotime( $date[1] );
   if ( $parsedDate === -1 )
   {
       // error parsing date, just return the current time
        return gmdate("U");
   }
   return $parsedDate;
}

function kde_news_dot ($file, $items, $alive = true)
{
  if (!$alive)
  {
?>
<div>
<h2><a name="news">Latest News</a></h2>
<p>dot.kde.org are currently not available.</p>
</div>
<?php
    return;
  }

  //How often to regenerate the html from the rdf in seconds
  $time           =       split(" ", microtime());
  print "<div>\n";
  global $site_locale;
  startTranslation($site_locale);
  print "<h2><a name=\"news\">" . i18n_var("Latest News") . "</a></h2>\n";

  $news= new RDF();
  $rdf_pieces = $news->openRDF($file);

  if(!$items) $items = 5; // default
  $rdf_items = count($rdf_pieces);
  if ($rdf_items > $items) $rdf_items = $items;

  //only open the file if it has something in it
  if ($rdf_items > 0)
  {
    $entry = "<table class=\"table_box\" summary=\"". i18n_var("This is a list of the latest news headlines shown on KDE Dot News") . "\">\n<tr><th>" . i18n_var("Date") . "</th>\n<th>" . i18n_var("Headline") . "</th></tr>\n";
    print $entry;

    for($x=1;$x<=$rdf_items;$x++)
    {
      preg_match("#<title>(.*)</title>#",$rdf_pieces[$x],$title );
      preg_match("#<link>(.*)</link>#",$rdf_pieces[$x],$links );
      preg_match("#<pubDate>(.*)</pubDate>#",$rdf_pieces[$x],$date );

      $title[1] = utf8_encode($title[1]);

      // Cut down the month to three letters format day and month to DD MMM
      $printDate2 = preg_replace("#[a-zA-Z]+,[[:blank:]]*([0-9]+)[[:blank:]]+([a-zA-Z][a-zA-Z][a-zA-Z]).*$#", "\\1&nbsp;\\2", $date[1]);

      $entry = "<tr>\n" . 
        "<td valign=\"top\" class=\"cell_date\"><b>$printDate2</b></td>\n" . 
        "<td><a href=\"$links[1]\" title=\"" . 
        i18n_var("Read the full article on dot.kde.org") . "\">" .
        $title[1]. "</a></td>\n" . "</tr>\n";

      print $entry;
    }

    $entry = "</table>\n" .
      "<p>View <a href=\"http://dot.kde.org/\" title=\"" .
      i18n_var("View older news items") . "\">" . i18n_var("more news...") .
      "</a></p>\n";

    print $entry;
  }

  print "</div>\n";
}

function kde_news_apps ($file, $items)
{
  global $site_locale;
  startTranslation($site_locale);
  $time           =       split(" ", microtime());
  echo "<div>\n";
  echo "<h2><a name=\"applications\">" . i18n_var("Latest Applications") . "</a></h2>\n";

  $apps = new RDF();
  $rdf_pieces = $apps->openRDF($file);

  if(!$items) $items = 5; // default
  $rdf_items = count($rdf_pieces);
  //if ($rdf_items > $items) $rdf_items = $items;

  if ($rdf_items > 0)
  {
    $entry = "<table class=\"table_box\" summary=\"" .
      i18n_var("List of latest third party application releases for KDE") .
       "\">\n<tr>\n<th>" . i18n_var("Date") . "</th>\n<th>" . 
       i18n_var("Application / Release") . "</th>\n</tr>";

    print $entry;

    $cutoff = gmdate("U", time() - 90000);

    $prevDate = "";
    for($x=1;$x<$rdf_items;$x++)
    {
      ereg("<title>(.*)</title>",$rdf_pieces[$x],$title );
      ereg("<program>(.*)</program>",$rdf_pieces[$x],$app );
      ereg("<version>(.*)</version>",$rdf_pieces[$x],$version );
      ereg("<date>(.*)</date>",$rdf_pieces[$x],$date );
      ereg("<link>(.*)</link>",$rdf_pieces[$x],$links );
      ereg("<KDEversion>(.*)</KDEversion>",$rdf_pieces[$x],$KDEversion );
      ereg("<description>(.*)</description>",$rdf_pieces[$x],$desc );
      ereg("<category>(.*)</category>",$rdf_pieces[$x],$category );

      // no valid date string around !
      if (strlen ($date[1]) < 31)
        $printDate = date ("d", gmdate("U"))."&nbsp;".date ("F", gmdate("U"));
      else
      {
        // get the "10 Jan" part of the date string
        $printDate = substr ($date[1], 5, strlen($date[1])-20);

        // get month number !
        $monthName = strtolower(substr ($printDate, 3, 3));
        switch ($monthName)
        {
          case "jan":
            $month = 1;
            break;
          case "feb":
            $month = 2;
            break;
          case "mar":
            $month = 3;
            break;
          case "apr":
            $month = 4;
            break;
          case "may":
            $month = 5;
            break;
          case "jun":
            $month = 6;
            break;
          case "jul":
            $month = 7;
            break;
          case "aug":
            $month = 8;
            break;
          case "sep":
            $month = 9;
            break;
          case "oct":
            $month = 10;
            break;
          case "nov":
            $month = 11;
            break;
          case "dec":
            $month = 12;
            break;
        }

        $monthName = date ("M", mktime(0,0,0,$month,1,0));

        // create the date string
        if ($printDate[0] == " ")
          $printDate = substr ($printDate, 1, 1)."&nbsp;".$monthName;
        else
          $printDate = substr ($printDate, 0, 2)."&nbsp;".$monthName;
      }

      $entry = "<tr>" .
        "<td valign=\"top\" class=\"cell_date\">".
        (($printDate == $prevDate) ? "&nbsp;" : "<b>$printDate</b>") .
        "</td>\n" .
        "<td valign=\"top\"><a href=\"$links[1]\">". $title[1] .
        " </a>\n" .
        "</td>\n</tr>" ;

      print $entry;

      $prevDate = $printDate;
      // algorithm:  show max(last 6, last 24 hours)
      // actually use a bit more than 24 hours (90000 secs)

      if (($x > $items) && (kde_parse_date ($date[1]) < $cutoff))
        break;
    }

    $entry = "</table>\n<p>" . i18n_var("View <a href=\"http://kde-apps.org/\">more applications...</a>") . "</p>\n<p style=\"font-size: 0.6em\"><em>" . i18n_var("Disclaimer: ") . "</em>" . i18n_var("Application feed provided by <a href=\"http://kde-apps.org/\">kde-apps.org</a>, an independent KDE website.") . "</p>\n";
    print $entry;
  }

  echo "</div>";
}
?>
