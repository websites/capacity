<?php // This file contains content between the top of the html body and the heading
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">

<!-- table row holding the header -->
<tr>
  <td>  
    <div id="nav_header_top" align="right">
      <a href="/"><img id="nav_header_logo" alt="Home" align="left" <?php print $site_logo_left ?> border="0" /></a>
    <?php if ($site_logo_right) { ?>
      <span class="doNotDisplay">::</span>
      <img id="nav_header_logo_right" alt="" align="right" <?php print $site_logo_right ?> border="0" />
    <?php } ?>
    
      <div id="nav_header_title" align="left"><?php print $site_title; ?></div>
    
   </div>
    
    <div id="nav_header_bottom" align="right">
      <span id="nav_header_bottom_right">
          <a href="<?php if ($site_external) print "http://www.kde.org" ?>/family/" accesskey="3" title="<?php i18n("A complete structural overview of the KDE.org web pages")?>"><?php i18n("Sitemap")?></a> ::
          <a href="<?php if ($site_external) print "http://www.kde.org" ?>/documentation/" accesskey="6" title="<?php i18n("Having problems? Read the documentation")?>"><?php i18n("Help")?></a> ::
          <a href="<?php if ($site_external) print "http://www.kde.org" ?>/contact/" title="<?php i18n("Contact information for all areas of KDE")?>"><?php i18n("Contact Us")?></a>
      </span>
      <span id="nav_header_bottom_left">
        <?php echo i18n ('Location').': '; $kde_menu->showLocation(); ?>
      </span>
    </div>
  </td>
</tr>

<!-- table row holding the menu + main content -->
<tr>
  <td>
    <table id="main" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <?php if ($site_menus > 0) { ?>
        <td valign="top" class="menuheader" height="0"></td>
      <?php } ?>
    
      <td id="contentcolumn" valign="top" <?php if ($site_menus > 0) print 'rowspan="2"'; ?> >
        <div id="content"><div style="width:100%;">
