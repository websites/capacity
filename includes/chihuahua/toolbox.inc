<?php
/**
* Chihuahua theme for Capacity
* @ver 0.1
* @license BSD License - www.opensource.org/licenses/bsd-license.php
*
* Copyright (c) 2012 Ingo Malchow <imalchow@kde.org>
* All rights reserved. Do not remove this copyright notice.
*/

 if (! isset ($hidelocation) || $hidelocation == false) { 
	echo "<div id=\"location\">";
  if( !isset( $site_logo ) || empty( $site_logo ) ) {
    $src = $url_root."/media/images/kde.png";
  }
  else {
    $src = $url_root."/".$site_logo;
  }
  echo "<img src=\"{$src}\" id=\"site_logo\" alt=\"Logo\" />";
	echo "<div class=\"toolboxtext\">";
	$plasmaMenu->breadCrumb();
	echo "</div>";
	echo "</div>\n";
}
?>
