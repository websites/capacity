<?php 
/**
* Chihuahua theme for Capacity
* @ver 0.1
* @license BSD License - www.opensource.org/licenses/bsd-license.php
*
* Copyright (c) 2012 Ingo Malchow <imalchow@kde.org>
* All rights reserved. Do not remove this copyright notice.
*/

// This file contains content for the html header
?>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <link rel="meta" href="https://www.kde.org/labels.rdf" type="application/rdf+xml" title="ICRA labels" />
  <meta name="trademark" content="KDE e.V." />
<?php
  if (isset($site_description)) {
    print "<meta name=\"description\" content=\"$site_description\" />\n";
  } else {
    print "<meta name=\"description\" content=\"KDE Homepage, KDE.org\" />\n";
  }
?>
  <meta name="MSSmartTagsPreventParsing" content="true" />
  <meta name="robots" content="all" />
  <meta name="no-email-collection" content="https://www.unspam.com/noemailcollection" />
<?php
  if (file_exists($site_root.'/favicon.ico')) {
    print "<link rel=\"shortcut icon\" href=\"".$site_root."/favicon.ico\" />";
    print "<link rel=\"icon\" href=\"".$site_root."/favicon.ico\" />";
  } else {
    print "<link rel=\"icon\" href=\"/media/images/favicon.ico\" />";
    print "<link rel=\"shortcut icon\" href=\"/media/images/favicon.ico\" />";
  }

  if (isset($rss_feed_link))
    print "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"$rss_feed_title\" href=\"$rss_feed_link\" />\n";
  if (isset($rss_feed2_link))
    print "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"$rss_feed2_title\" href=\"$rss_feed2_link\" />\n";

  if (isset ($site_root) && file_exists (dirname ($_SERVER ['SCRIPT_FILENAME']).'/'.$site_root.'site.png')) {
    $imagepath = dirname ($_SERVER ['SCRIPT_NAME']);
    if (substr ($imagepath, -1, 1) != "/")
      $imagepath .= "/";
    if ($site_root != './')
      $imagepath .= $site_root;
    $cssparameters = "&amp;site-image=".urlencode ($imagepath.'site.png');
  }
  elseif (file_exists ($_SERVER ['DOCUMENT_ROOT'].'/site.png'))
    $cssparameters = "&amp;site-image=".urlencode ('/site.png');
  else
    $cssparameters = "";

  if (isset ($site_root) && file_exists (dirname ($_SERVER ['SCRIPT_FILENAME']).'/'.$site_root.'css.inc')) {
    $cssparameters .= "&amp;css-inc=".urlencode (dirname ($_SERVER ['SCRIPT_NAME']).'/'.$site_root);
  }
  elseif (file_exists ($_SERVER ['DOCUMENT_ROOT'].'/css.inc'))
    $cssparameters .= "&amp;css-inc=/";
?>

  	<link rel="stylesheet" media="screen" type="text/css" title="KDE Stylesheet" href="/media/includes/chihuahua/css.php" />
	<link rel="stylesheet" media="screen" type="text/css" title="KDE Stylesheet" href="/media/styles/plasmaMenu.css" />
	<?php $plasmaMenu->showCss(); ?>
	<!--[if lte IE 7]>
		<style type="text/css">
			html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
		</style>
	<![endif]-->
