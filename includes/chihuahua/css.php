<?php
header("Content-type: text/css");
/**
* Chihuahua theme for Capacity
* @ver 0.1
* @license BSD License - www.opensource.org/licenses/bsd-license.php
*
* Copyright (c) 2012 Ingo Malchow <imalchow@kde.org>
* All rights reserved. Do not remove this copyright notice.
*/
?>

/* Eric Meyer CSS Reset */
html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,code,del,dfn,em,font,img,ins,kbd,q,s,samp,small,strike,strong,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,legend,table,caption,tbody,tfoot,thead,tr,th,td,hr {
   margin: 0;
   padding: 0;
   border: 0;
   outline: 0;
   font-size: 100%;
   vertical-align: baseline;
   background: transparent;
}

body {
   line-height: 1;
}

ol,ul {
   list-style: none;
}

blockquote,q {
   quotes: none;
}

blockquote:before,blockquote:after,q:before,q:after {
   content: '';
   content: none;
}

big {
   font-size: 120%;
}

/* remember to define focus styles! */
:focus {
   outline: 0;
}

/* remember to highlight inserts somehow! */
ins {
   text-decoration: none;
}

del {
   text-decoration: line-through;
}

/* tables still need 'cellspacing="0"' in the markup */
table {
   border-collapse: collapse;
   border-spacing: 0;
}

body {
   text-align: center;
   font-family: "Liberation Sans","Bitstream Vera Sans",sans-serif;
}

/*Header and footer backgrounds*/
html {
   background: #fff;
/*#d4e9f1 #c2dfec;*/
background-image:url(../../images/kde1.jpg);
   background-repeat: no-repeat;
   background-position: top center;
}

body {
   background: transparent;
   background-image: url(../../images/footer1.jpg);
   background-repeat: no-repeat;
   background-position: bottom center;
}

/*Formating and setups*/
.root {
   width: 850px;
   margin: 0 auto;
   text-align: left;
   padding-bottom: 25px;
   min-height: 1100px;
}

.header {
   background-image: url(../../images/page-hr-alt.png);
   background-repeat: no-repeat;
   background-position: 0 28px;
}

.header .toolbox {
   height: 32px;
   margin-bottom: 8px;
}

.header .toolbox .toolboxtext {
   padding-top: 10px;
}

.header .toolbox img {
   padding-top: 4px;
   padding-right: 10px;
   float: left;
}

.header .toolbox div {
   float: left;
   width: auto;
   padding: 0 10px;
   color: #888;
   text-shadow: #fff 0 0 3px;
   text-decoration: none;
   font-size: 12px;
   font-weight: 400;
}

.header .toolbox div.toolboxtext {
   float: none;
}


.header .toolbox #location {
   min-width: 700px;
}

.header .toolbox #location a {
   color: #666;
   text-shadow: #fff 0 0 3px;
   text-decoration: none;
   font-size: 12px;
   font-weight: 400;
}

.header .toolbox #location a:hover {
   text-decoration: underline;
}

.header .menu_box {
   display: table;
   text-align: left;
   width: auto;
   min-height: 160px;
   margin: 0 auto;
   text-shadow: #fff 0 0 3px;
}

.header .menu_box li {
   float: left;
   margin: 3px 14px;
}

.header .menu_box li a {
   text-decoration: none;
   margin: 0 10px;
}

.header .menu_box li h2 a {
   color: #555;
   font-size: 14px;
   font-weight: 400;
   padding-left: 10px;
   text-transform: uppercase;
}

.header .menu_box li ul {
   padding-top: 5px;
   padding-left: 10px;
}

.header .menu_box li a:hover h2,.header .menu_box li a:hover,.header .menu_box li a#current {
   color: #222;
   text-shadow: #E6F1FF 0 0 3px;
   background-image: url(../../images/underline.png);
   background-repeat: repeat-x;
   background-position: bottom;
}

.header .menu_box li ul li {
   float: none;
   text-align: left;
   margin: 5px 0;
}

.header .menu_box li ul li a {
   color: #666;
   font-size: 12px;
   margin: 5px;
}

.header .menu_box li ul li ul {
   margin: 4px 2px 4px 10px;
   padding: 0;
}

.header .menu_box li ul li ul li {
   margin: 2px;
   padding: 0;
}

.header .menubox_body .menubox_title {
   font-weight: bold;
}

/*Header glow/icons*/
#cp-menu-home {
   background-image: url(../../images/blue.icon.png);
   background-repeat: no-repeat;
   background-position: 0 0;
}

#cp-menu-home:hover {
   text-shadow: #8ed1ff 0 0 18px;
}

#cp-menu-community {
   background-image: url(../../images/green.icon.png);
   background-repeat: no-repeat;
   background-position: 0 0;
}

#cp-menu-community:hover {
   text-shadow: #acff08 0 0 18px;
}

#cp-menu-workspaces {
   background-image: url(../../images/orange.icon.png);
   background-repeat: no-repeat;
   background-position: 0 0;
}

#cp-menu-workspaces:hover {
   text-shadow: #ffae00 0 0 18px;
}

#cp-menu-applications {
   background-image: url(../../images/red.icon.png);
   background-repeat: no-repeat;
   background-position: 0 0;
}

#cp-menu-applications:hover {
   text-shadow: #ff96af 0 0 18px;
}

#cp-menu-developerplatform {
   background-image: url(../../images/gray.icon.png);
   background-repeat: no-repeat;
   background-position: 0 0;
}

#cp-menu-developerplatform:hover {
   text-shadow: #aaa 0 0 18px;
}

#cp-menu-support {
   background-image: url(../../images/purple.icon.png);
   background-repeat: no-repeat;
   background-position: 0 0;
}

#cp-menu-support:hover {
   text-shadow: #e285ff 0 0 18px;
}

.content {
   background-image: url(../../images/page-bg-alt.png);
   background-repeat: repeat-y;
   background-position: top center;
}

.content .teaser,.content .teaser_edu {
   height: 300px;
   width: 830px;
   display: block;
   margin: auto;
}

.content .teaser_hide {
   display: none;
}

.content a {
   color: #235E9A;
}

.footer {
   text-align: left;
   background-image: url(../../images/page-fr-alt.png);
   background-repeat: no-repeat;
   background-position: 0 0;
   width: 850px;
   height: 60px;
   padding: 40px 0 0;
}

.footer .module {
   float: left;
   width: 25%;
   margin: 0 10px 30px;
}

.footer .module {
   font-size: x-small;
}

#footer {
   color: #aaa;
   font-size: xx-small;
   width: 100%;
   background: #333;
   padding: 5px 0;
   margin-top: -25px;
   height: 25px;
}

#footer a {
   color: #eee;
}

#sidebar {
   color: #444;
   float: right;
   max-width: 185px;
   min-height: 200px;
   margin: 10px 0 10px 15px;
   padding: 15px;
   border: 1px solid #ccc;
   font-size: 10pt;
   text-align: left;
   background-image: url(../../images/30.png);
   border-radius: 3px;
   -moz-border-radius: 3px;
   -webkit-border-radius: 3px;
   -khtml-border-radius: 3px;
   box-shadow: 0 1px 8px #aaa;
}

a.cp-doNotDisplay {
   display: none;
}

#main {
   text-align: justify;
   padding: 10px 35px 0;
   font-size: 10pt;
   line-height: 120%;
   text-shadow: #fff 0 0 3px;
   color: #444;
}

#main a[href] {
   color: #235E9A;
   text-decoration: none;
}

#main a[href]:hover {
   text-decoration: underline;
}

#main p {
   color: #444;
   padding: 10px 0;
}

#main h1,#main h2,#main h3,#main h4,#main h5,#main h6 {
   font-weight: 400;
}

#main h1 {
   line-height: 130%;
   padding-left: 10px;
   background-image: url(../../images/30.png);
   color: #335877;
   font-size: 18pt;
   margin: 20px 10px 10px 0px;
}

#main h2 {
   color: #446888;
   font-size: 16pt;
   padding: 30px 0 20px 0px;
   line-height: 120%;
}

#main h3 {
   color: #557899;
   font-size: 15pt;
   padding: 25px 0 15px 0px;
   line-height: 120%;
}

#main h4 {
   color: #557899;
   font-size: 13pt;
   padding: 20px 0 10px 0px;
   font-weight: 700;
   line-height: 120%;
}

#main h5 {
   color: #557899;
   font-size: 12pt;
   padding: 15px 0 10px 0px;
   font-weight: 700;
   line-height: 120%;
}

#main h6 {
   color: #557899;
   font-size: 11pt;
   padding: 15px 0 10px 0px;
   font-weight: 700;
   line-height: 120%;
}

#main > h1,#main > h2,#main > h3,#main > h4,#main > h5,#main > h6 {
   margin-left: -15px;
}

#main img {
   margin: 10px;
   padding: 5px;
   background-image: url(../../images/70.png);
   border: 1px solid #aaa;
   border-radius: 3px;
   -moz-border-radius: 3px;
   -webkit-border-radius: 3px;
   -khtml-border-radius: 3px;
}

#main img.clear {
   background-image: none;
   border: 0;
   padding: 0;
   margin: 0;
}

#main dl,#main ol,#main ul {
   padding: 5px 30px;
}

#main ol {
   list-style: decimal;
}

#main ul {
   list-style-image: url(../../images/list-circle.png);
}

#main ul li:hover {
   list-style-image: url(../../images/list-circle-over.png);
}

#main ul li ul {
   padding: 10px 0 0 30px;
}

#main ul.clear, #main ol.clear,
#main ul.clear li:hover, #main ol.clear li:hover {
   list-style: none;
   list-style-image: none;
}

#main li {
   padding-bottom: 10px;
}

#main form {
   margin: 10px;
   padding: 5px;
   border: 1px solid #E1EAF2;
   box-shadow: 0 2px 3px #888;
   border-radius: 5px;
   -moz-border-radius: 5px;
   -webkit-border-radius: 5px;
   -khtml-border-radius: 5px;
}

#main #languageChooser form {
	float: right;
	font-size: 10pt;
	text-align: left;
	background-image: url(../../images/30.png);
	margin: 20px 10px 10px 10px;
}

#main fieldset {
}

#main legend {
}

#main input {
}

#main textarea {
}

#main table {
   border: 1px solid #E1EAF2;
   color: #444;
   background-image: url(../../images/30.png);
   border-radius: 3px;
   -moz-border-radius: 3px;
   -webkit-border-radius: 3px;
   -khtml-border-radius: 3px;
}

#main table th {
   color: #444;
   background-color: #E1EAF2;
   padding: 5px;
   text-align: center;
}

#main table tr:nth-child(odd) {
   background-image: url(../../images/30.png);
}

#main table tr:nth-child(even) {
}

#main table tr:hover {
   background-image: url(../../images/30s.png);
}

#main table td {
   padding: 5px;
}

#main table.donations-and-sponsors td {
   vertical-align: middle;
}

#main sup {
   font-size: 7pt;
}

#main sub {
   font-size: 7pt;
}

#main cite {
}

#main pre {
   font-size: 9pt;
   font-style: monospace;
   color: #555;
   padding: 2px 5px 2px 20px;
   border: 1px solid #DDD;
   margin-left: 20px;
}

/* Download button */
#main a.downloadButton {
   display: block;
   width: 130px;
   height: 61px;
   background: url(/images/buttons/download_empty.png);
   text-align: center;
   padding: 40px 10px 15px 130px;
   font-size: 12pt;
   line-height: 20px;
   margin-left: auto;
   margin-right: auto;
   vertical-align: middle;
   color: #444;
}

#main a.downloadButton:hover {
   text-decoration: none;
}

.table-wrapper {
   padding: 15px;
}

/* Application pages */
#sidebar a {
   text-decoration: none;
}

.app-icon {
   float: left;
}

.app-screenshot {
   clear: left;
   text-align: center;
}

.infobox {
   padding-top: 15px;
}

#infobox-return img,.header-image {
   padding: 0!important;
   border: 0!important;
   margin: 0 5px 0 0!important;
   background-image: none!important;
}

.app-category,.international-site {
   float: left;
   display: table-cell;
   max-width: 240px;
   width: 240px;
   height: 60px;
   max-height: 60px!important;
   text-align: left;
}

.app-category img,.international-site img {
   float: left;
   margin: 0 10px 0 0!important;
}

.app-category a,.international-site a {
   font-size: 12pt;
   font-weight: 700;
   text-decoration: none;
}

.international-site {
   height: auto!important;
}

/*
   OCS Stuff
*/
.info-area {
   float: right;
   max-width: 300px;
   padding-left: 10px;
   text-align: left;
}

#main table.ocs img {
   padding: 1px!important;
   margin: 0;
}

#main table.ocs {
   border: none!important;
}

.ocs-hackergotchi img {
   height: 32px!important;
   width: 32px!important;
}

.ocs-applicationshot {
   padding: 10px 0 10px 10px!important;
}

.ocs-latestapp {
   padding: 10px!important;
}

.ocs-content,.ocs-linksbar,.ocs-latestapp {
   vertical-align: top;
   text-align: left;
}

.ocs-title {
   font-weight: 700;
   padding-right: 10px;
}

.ocs-linksbar {
   text-align: right!important;
   min-width: 75px;
}

.ocs-linksbar img {
   margin: 0 2px 2px!important;
}

.screenshot {
   margin-top: 20px;
   margin-bottom: 20px;
}

.main-content .app-screenshot {
	max-width: 539px;
}

.toggle {
   float: right;
   width: auto;
   padding: 10px 10px 0;
   color: #888;
   text-shadow: #fff 0 0 3px;
   text-decoration: none;
   font-size: 12px;
   font-weight: 400;
}

#hotspot {
   float: right;
}

#main .social img {
	margin: 5px;
	vertical-align: top;
}

#main table.social {
	border: 0;
	width: 100%;
}

#main table.thanks {
	text-align: left;
}

#main table.thanks td {
	border: 1px solid #E1EAF2;
}

.timeline-members td {
	border: 1px solid #dddddd;
}
