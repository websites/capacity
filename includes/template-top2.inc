<?php // This file contains content between the heading and the page text
    if (isset($site) and ($site == "developer"))
    {
        print ("<p style='border-width:medium; border-color:#f00; border-style:solid; padding:5px;'><b>Note:</b> This page has been superseeded by <a href='http://techbase.kde.org'>KDE TechBase</a>. If you are looking for up-to-date information on KDE development or system administration information, look there. If you find that the content of this page is not available in TechBase, please create the page yourself, it is a Wiki!</p>");
    }
?>
