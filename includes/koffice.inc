<?php
include_once ("functions.inc");

$site_title = "The KOffice Project";
//$site_logo_right = siteLogo("/media/images/koffice.png", "64", "64");
$site_external = true;
$name = "koffice.org Web Team";
$mail = "webmaster@kde.org";
$currentversion='1.6';
$develversion='2.0';
$koshellversion='1.6.0';
$kwordversion='1.6.0';
$kspreadversion='1.6.0';
$kpresenterversion='1.6.0';
$kritaversion='1.6.0';
$kchartversion='1.6.0';
$kformulaversion='1.6.0';
$kugarversion='1.6.0';
$kivioversion='1.6.0';
$karbonversion='1.6.0';
$kexiversion='1.1.0';
$kplatoversion='0.6.0';
?>
