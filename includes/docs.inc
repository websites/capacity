<?php
include_once ("functions.inc");
$site_title = "KDE Documentation";
$site_logo_right = siteLogo("/media/images/docs.png", "64", "64");
$site_external = true;
$name="Rainer Endres";
$mail="endres@kde.org";

include_once ("include/docs_classes.inc");

$kde_stylesheets = new StylesheetList ();
$kde_stylesheets->push("Docs Default","/media/styles/standard.css;/docs.css");

?>
