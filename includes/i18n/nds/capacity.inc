<?php
$text["Maintained by"] = "Pleegt vun";
$text["Maintained by <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">The KDE Webmaster</a><br />\n"] = "Pleegt vun <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">De KDE-Nettpleger</a><br />\n";
$text["Search:"] = "Söken:";
$text["Search"] = "Söken";
$text["Language:"] = "Spraak:";
$text["Change language"] = "Spraak ännern";
$text["<b>Version %1</b>"] = "<b>Verschoon %1</b>";
$text["Questions"] = "Fragen";
$text["Answers"] = "Antwoorden";
$text["Skip menu \"%1\""] = "Menü \"%1\" övergahn";
$text["Latest News"] = "Niegste Narichten";
$text["Date"] = "Datum";
$text["Headline"] = "Överschrift";
$text["Latest Applications"] = "Niegste Programmen";
$text["KDE Home"] = "KDE-Nettsiet";
$text["Back to menu"] = "Torüch na't Menü";
$text["K Desktop Environment"] = "K-Schriefdischümgeven";
$text["Y-m-d"] = "J-M-D";
$text["kde.org"] = "kde.org";
$text["developer.kde.org"] = "developer.kde.org";
$text["kde-look.org"] = "kde-look.org";
$text["Applications"] = "Programmen";
$text["Documentation"] = "Dokmentatschoon";
$text["Sitemap"] = "Siedenstruktuur";
$text["Help"] = "Hülp";
$text["Contact Us"] = "Snack uns an";
$text["Location"] = "Steed";
?>

