<?php
$text["Maintained by"] = "Správce";
$text["Search:"] = "Hledat:";
$text["Search"] = "Hledat";
$text["Language:"] = "Jazyk:";
$text["Donate:"] = "Přispějte:";
$text["Donate"] = "Přispět";
$text["Legal"] = "Legal";
$text["KDE<sup>&#174;</sup> and <a href=\"/media/images/trademark_kde_gear_black_logo.png\">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href=\"http://ev.kde.org/\" title=\"Homepage of the KDE non-profit Organization\">KDE e.V.</a>"] = "KDE<sup>&#174;</sup> a <a href=\"/media/images/trademark_kde_gear_black_logo.png\">K Desktop Environment<sup>&#174;</sup> logo</a> jsou registrované obchodní známky <a href=\"http://ev.kde.org/\" title=\"Homepage of the KDE non-profit Organization\">KDE e.V.</a>";
$text["DONATE"] = "PŘISPĚT";
$text["Authors:"] = "Autoři:";
$text["Thanks To:"] = "Díky patří:";
$text["Browse %1 source code online"] = "Prohledávejte zdrojové kódy %1 na síti";
$text["<b>Version %1</b>"] = "<b>Verze %1</b>";
$text["Questions"] = "Otázky";
$text["Answers"] = "Odpovědi";
$text["Latest News"] = "Nejnovější zprávy";
$text["Date"] = "Datum";
$text["Headline"] = "Nadpis";
$text["Disclaimer: "] = "Podmínky: ";
$text["Back to menu"] = "Zpět do nabídky";
$text["K Desktop Environment"] = "K Desktop Environment";
$text["kde.org"] = "kde.org";
$text["developer.kde.org"] = "developer.kde.org";
$text["kde-look.org"] = "kde-look.org";
$text["Applications"] = "Aplikace";
$text["Documentation"] = "Dokumentace";
$text["Hotspot"] = "Hotspot";
$text["KDE<sup>&#174;</sup> and <a href=\"/media/images/kde_gear_black.png\">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href=\"http://ev.kde.org/\" title=\"Homepage of the KDE non-profit Organization\">KDE e.V.</a>"] = "KDE<sup>&#174;</sup> a <a href=\"/media/images/kde_gear_black.png\">K Desktop Environment<sup>&#174;</sup> logo</a> jsou registrované obchodní známky <a href=\"http://ev.kde.org/\" title=\"Homepage of the KDE non-profit Organization\">KDE e.V.</a>";
$text["Help"] = "Nápověda";
$text["Contact Us"] = "Kontaktujte nás";
$text["Location"] = "Umístění";
?>

