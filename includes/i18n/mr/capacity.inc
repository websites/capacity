<?php
$text["Search:"] = "शोधा :";
$text["Search"] = "शोधा";
$text["Language:"] = "भाषा :";
$text["Legal"] = "Legal";
$text["Authors:"] = "लेखक :";
$text["Thanks To:"] = "यांस धन्यवाद :";
$text["<b>Version %1</b>"] = "<b>आवृत्ती %1</b>";
$text["Questions"] = "प्रश्न";
$text["Answers"] = "उत्तरे";
$text["Date"] = "दिनांक";
$text["kde.org"] = "kde.org";
$text["Applications"] = "अनुप्रयोग";
$text["Documentation"] = "दस्तऐवजीकरण";
$text["Help"] = "मदत";
$text["Location"] = "स्थान";
?>

