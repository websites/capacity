<?php
$text["Search:"] = "Cuardaigh:";
$text["Search"] = "Cuardaigh";
$text["Language:"] = "Teanga:";
$text["Legal"] = "Dlí";
$text["Authors:"] = "Údair:";
$text["Questions"] = "Ceisteanna";
$text["Answers"] = "Freagraí";
$text["Latest News"] = "Nuacht is déanaí";
$text["Date"] = "Dáta";
$text["Headline"] = "Ceannlíne";
$text["Applications"] = "Feidhmchláir";
$text["Documentation"] = "Doiciméadú";
$text["Help"] = "Cabhair";
$text["Contact Us"] = "Teagmháil";
$text["Location"] = "Suíomh";
?>

