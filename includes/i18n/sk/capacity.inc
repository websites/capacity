<?php
$text["Maintained by"] = "Správca";
$text["Search:"] = "Hľadať:";
$text["Search"] = "Hľadať";
$text["Language:"] = "Jazyk:";
$text["Change language"] = "Zmeniť jazyk";
$text["Donate:"] = "Venovať peniaze:";
$text["(Why?)"] = "(Prečo?)";
$text["Donate"] = "Venovať peniaze";
$text["Legal"] = "Legal";
$text["DONATE"] = "Venovať peniaze";
$text["Authors:"] = "Autori:";
$text["Thanks To:"] = "Poďakovanie:";
$text["<b>Version %1</b>"] = "<b>Verzia %1</b>";
$text["Questions"] = "Otázky";
$text["Answers"] = "Odpovede";
$text["Skip menu \"%1\""] = "Preskočiť ponuku \"%1\"";
$text["Latest News"] = "Posledné novinky";
$text["Date"] = "Dátum";
$text["Headline"] = "Titulok";
$text["more news..."] = "viac noviniek...";
$text["Latest Applications"] = "Najnovšie aplikácie";
$text["Application / Release"] = "Aplikácia / vydanie";
$text["Disclaimer: "] = "Podmienky:";
$text["KDE Home"] = "KDE domov";
$text["Back to content"] = "Späť na obsah";
$text["Back to menu"] = "Späť na ponuku";
$text["K Desktop Environment"] = "K Desktop Environment";
$text["Skip to content"] = "Preskočiť na obsah";
$text["Y-m-d"] = "Y-m-d";
$text["kde.org"] = "kde.org";
$text["developer.kde.org"] = "developer.kde.org";
$text["kde-look.org"] = "kde-look.org";
$text["Applications"] = "Aplikácie";
$text["Documentation"] = "Dokumentácia";
$text["Hotspot"] = "Hotspot";
$text["Sitemap"] = "Mapa siete";
$text["Help"] = "Pomocník";
$text["Contact Us"] = "Kontaktujte nás";
$text["Location"] = "Umiestnenie";
?>

