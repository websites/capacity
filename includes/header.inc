<?php
  // include functions we need in the normal case
  // this must be first thing we do, as site.inc may already use this set of functions
  include_once ("functions.inc");

  // setup $site_root; $document_root; $url_root; $current_relativeurl;
  setup_site ();

  // take a look: have we any locale.inc there (to hardcode language for locale pages)?
  if (file_exists ("$document_root/locale.inc"))
    include_once ("$document_root/locale.inc");

  // do some processing based on the servername
  $servername = $_SERVER['SERVER_NAME'];

  // the cookie domain we use
  $cookiedomain = "." . $servername;

  // if a locale is set, this overwrites all other stuff!!!!
  if (!isset($site_locale))
  {
    if (isset($_GET['site_locale']))
    {
      $site_locale = htmlentities($_GET['site_locale']);
      setcookie('site_locale', $site_locale, time()+31536000, '/', $cookiedomain);
    }
    else
    {
      if (isset($_COOKIE['site_locale']))
      {
        $site_locale = htmlentities($_COOKIE['site_locale']);
      }
      else
      {
        if (!isset($default_site_locale))
        {
          $site_locale = "en";
        }
        else
        {
          $site_locale = $default_site_locale;
        }
      }
    }
  }

  // set php locale
  setlocale(LC_ALL, $site_locale);

  // start up our i18n stuff
  startTranslation($site_locale);

  // IMPORTANT: now that i18n are up and running, include the site specific config include
  // include the site.inc of this page! this is a more sane way than having this xyz.inc's in the include dir!
  if (file_exists ("$document_root/site.inc"))
    include_once ("$document_root/site.inc");

  // figure out if we are external
  if (isset($site) && $site != "www")
    $site_external = true;

  // left corner logo, e.g. KDE Crystal Gear
  if (!isset($site_logo_left))
    $site_logo_left = siteLogo("/media/images/kde_gear_64.png", "64", "64");

  // count of menu sidebars
  if (!isset ($site_menus))
    $site_menus = 1;

  // create menu if not already around!
  if (!isset($kde_menu))
  {
    // now try to find right menu implementation for this page
    if (isset($site) and ($site == "developer"))
      include_once("classes/class_menu_developer.inc");
    else
      include_once("classes/class_menu.inc");

    // construct the menu
    $kde_menu = new BaseMenu ($document_root, $url_root, $current_relativeurl);
	
	// include the plasmaMenu entries
	if (file_exists("$document_root/plasmaMenu.inc"))
	{
		$plasmaMenu = new plasmaMenuBuilder();
		include_once("$document_root/plasmaMenu.inc");
	}
  }
  
  // initialize the teaser class and it's object
  /*include_once("classes/class_teaser.inc");
  $teaserImages = new teaserImageBuilder();
*/
  if (isset($_REQUEST['sitestyle']))
    $site_style = $_REQUEST['sitestyle'];
  else if (isset($_REQUEST['site_style']))
    $site_style = $_REQUEST['site_style'];
  else $site_style = "windowcolors";

  if (!isset($site_title))
    $site_title = i18n_var("K Desktop Environment");

  if (function_exists('function_to_get_page_title'))
    $page_title = function_to_get_page_title();

  if (isset($page_title))
    $title = "$site_title - ".i18n_var( $page_title );
  else
    $title = $site_title;

  if (! isset ($templatepath))
    $templatepath = "";
	
?>
<!DOCTYPE html>
<html lang="<?php print $site_locale; ?>">
<head>
  <meta charset="UTF-8" />
  <title><?php print $title; ?></title>

<style type="text/css">
  .cp-doNotDisplay { display: none; }
  @media aural, braille, handheld, tty { .cp-doNotDisplay { display: inline; speak: normal; }}
  .cp-edit { text-align: right; }
  @media print, embossed { .cp-edit { display: none; }}
</style>

<?php
if (isset ($templatepath))
    include $templatepath."template-header.inc";
else
    include "template-header.inc";

if (file_exists ("$document_root/site-header.inc"))
  include_once ("$document_root/site-header.inc");
?>

</head>

<body <?php
echo ' id="cp-site-'.htmlspecialchars (str_replace(".", "", $servername)).'"';
if(isset($rtl) && $rtl) print " dir=\"rtl\"";
if(isset($body_onload)) print " onload=\"".$body_onload."\"";
if(isset($body_onunload)) print " onunload=\"".$body_onunload."\"";
?>>

<ul class="cp-doNotDisplay">
  <li><a href="#cp-content" accesskey="2"><?php i18n("Skip to content")?></a></li>
  <li><a href="#cp-menu" accesskey="5"><?php i18n("Skip to link menu")?></a></li>
</ul>


<?php
if (isset ($templatepath))
    include $templatepath."template-top1.inc";
else
    include "template-top1.inc";
?>
<a name="cp-content"></a>
<?php
  if (isset($page_title) && ($page_title != "")) {
    if (isset($page_title_extra_html)) {
      print $page_title_extra_html;
    }
    print "<h2>".i18n_var( $page_title )."</h2>\n";
  }
  print "<div style='clear:both;'></div>\n";

if (isset ($templatepath))
    include $templatepath."template-top2.inc";
else
    include "template-top2.inc";

?>
