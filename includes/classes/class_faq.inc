<?php

/**
 * Written by Chris Howells <howells@kde.org>
 * Based on code by Christoph Cullmann <cullmann@kde.org>
 * Based on the cool menu code from usability.kde.org written by Simon Edwards <simon@simonzone.com>
 */

class FAQ
{
	var $items = array();
	var $question;
	var $section;

	function __construct()
	{
	}

	function addQuestion($question_text, $answer_text, $anchor = "")
	{
		$question = new Question($question_text, $answer_text, $anchor);
		array_push($this->items, $question);
	}

	function addSection($section_name)
	{
		$section = new Section($section_name);
		array_push($this->items, $section);
	}

	function show()
	{
		global $site_locale;
		startTranslation($site_locale);

		if ( count($this->items) )
		{
			$numitems = count( $this->items );
			print "\n<h2><a name=\"questions\">". i18n_var("Questions") . "</a></h2>\n";
			$isListOpened = false;                        
			for ($i=0; $i < $numitems; $i++)
			{
				$needOpenList = $this->items[$i]->needsList();
				if ( ! $isListOpened && $needOpenList )
				{
					print "<ul>\n";
					$isListOpened = true;
				}
				elseif  ( $isListOpened && ! $needOpenList )
				{
					print "</ul>\n";
					$isListOpened = false;
				}
				$this->items[$i]->showQuestion();
			}
			if ( $isListOpened )
				print "</ul>\n";

			print "\n\n<h2>" . i18n_var("Answers") . "</h2>\n";

			for ($i=0; $i < $numitems; $i++)
			{
				$this->items[$i]->showAnswer();
			}
		}
	}
}

class Question
{
	var $question;
	var $answer;
	var $anchor;

	function __construct($question, $answer, $anchor = "")
	{
		$this->question = $question;
		$this->answer = $answer;
		if ($anchor == "")
			$this->anchor = preg_replace("/[^a-zA-Z]/", "", $this->question);
		else
			$this->anchor = $anchor;
	}

	function showQuestion()
	{
		print "<li><a href=\"#$this->anchor\">$this->question</a></li>\n";
	}

	function showAnswer()
	{
		global $site_locale;
		print "<h4><a name=\"$this->anchor\">$this->question</a></h4>\n";
		print "<p>$this->answer</p>\n";
		print "<p><a href=\"#questions\">". i18n_var("[Up to Questions]") . "</a></p>\n";
	}

	function needsList()
	{
		return true;
	}
}

class Section
{
	var $section;

	function __construct($section)
	{
		$this->section = $section;
	}

	function showQuestion()
	{
		print "\n<h3>$this->section</h3>\n";
	}

	function showAnswer()
	{
		print "\n<h3>$this->section</h3>\n";
	}

	function needsList()
	{
		return false;
	}
}
