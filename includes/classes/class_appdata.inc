<?php

/**
 * Reads a json file containing information for an application page on kde.org and gives
 * access to its data.
 * Written by Daniel Laidig <d.laidig@gmx.de>
 *
 * Usage:
 * <?php
 *  TODO
 * ?>
 */

class AppData
{
	var $data;

        function AppData($name, $path = "..") {
            $name = preg_replace('/[^a-z\-_0-9]/i','',$name);
            $this->data = json_decode(file_get_contents($path.'/applications/apps/'.$name.'.json'), true) + json_decode(file_get_contents($path.'/applications/apps/'.$name.'_generated.json'), true);
        }

        function name() {
            return $this->data['name'];
        }

        function genericName() {
            return $this->data['generic name'];
        }

        function category() {
            return $this->data['category'];
        }

        function descriptionHtml() {
            $description = $this->data['description'];
            $description = i18n_var($description);
            if(substr($description, 0, 3) != '<p>') {
                $description = '<p>'.$description.'</p>';
            }
            if ($this->category() == "Unmaintained") {
                $description .= "<p><b>".i18n_var('%1 is unmaintained and no longer released by the KDE community.', $this->name())."</b></p>";
            }
            return $description;
        }

        function listToHtml($list) {
            $html = '<ul class="features">';
            $liOpen = false;
            foreach($list as $item) {
                if (is_array($item)) {
                    $html .= $this->listToHtml($item);
                    continue;
                }
                if ($liOpen)
                    $html .= '</li>';
                $html .= '<li>'.i18n_var($item);
                $liOpen = true;
            }
            $html .= '</li></ul>';
            return $html;
        }

        function hasFeatures() {
            return $this->data['features'] != false;
        }

        function featureHtml() {
            if (is_array($this->data['features'])) {
                return $this->listToHtml($this->data['features']);
            } else {
                return $this->data['features'];
            }
        }

        function hasVersions() {
            return false; ///TODO: version information is not yet implemented
        }

        function hasAuthors() {
            return isset($this->data['authors']) && ($this->data['authors'] != false);
        }

        function authorHtml() {
            $html = '<p><strong>'.i18n_var( "Authors:" ).'</strong></p>';
            $html .= '<ul>';
            foreach($this->data['authors'] as $author) {
                $html.= '<li><span>'.$author[0].'</span>';
                if (strlen($author[2])) {
                    $html .= ' <a href="mailto:'.htmlspecialchars($author[2]).'">&lt;'.htmlspecialchars($author[2]).'&gt;</a>';
                }
                $html.='<br />'.i18n_var($author[1]);
                if (strlen($author[3])) {
                    $html .= '<br /><a href="'.htmlspecialchars($author[3]).'">'.htmlspecialchars($author[3]).'</a>';
                }
                $html .= '</li>';
            }
            $html .= '</ul>';
            if (count($this->data['credits'])) {
                $html .= '<p><strong>'.i18n_var( "Thanks To:" ).'</strong></p>';
                $html .= '<ul>';
                foreach($this->data['credits'] as $author) {
                    $html.= '<li><span>'.$author[0].'</span>';
                    if (strlen($author[2])) {
                        $html .= ' <a href="mailto:'.htmlspecialchars($author[2]).'">&lt;'.htmlspecialchars($author[2]).'&gt;</a>';
                    }
                    $html.='<br />'.i18n_var($author[1]);
                    if (strlen($author[3])) {
                        $html .= '<br /><a href="'.htmlspecialchars($author[3]).'">'.htmlspecialchars($author[3]).'</a>';
                    }
                    $html .= '</li>';
                }
                $html .= '</ul>';
            }
            return $html;
        }
        
        function hasLicense() {
            return isset($this->data['license']) && ($this->data['license'] != false);
        }

        function licenseHtml() {
            $license = $this->data['license'];
            $text = '<p>';
            switch($license) {
                case 'GPL': case 'GPL_V2':
                    $text .= i18n_var('%1 is distributed under the terms of the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU General Public License (GPL), Version 2</a>.', $this->name());
                    break;
                case 'GPL_V3':
                    $text .= i18n_var('%1 is distributed under the terms of the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License (GPL), Version 3</a>.', $this->name());
                    break;
                case 'LGPL':
                    $text .= i18n_var('%1 is distributed under the terms of the <a href="http://www.gnu.org/licenses/old-licenses/lgpl-2.0.html"> GNU Library General Public License, version 2</a>.', $this->name());
                    break;
                default:
                    $text .= $license.'.';
            }
            $text .= '</p>';
            return $text;
        }

        function hasHomepage() {
            return $this->data['homepage'] != false;
        }
        function homepage() {
            return $this->data['homepage'];
        }

        function hasAppStream() {
            return isset($this->data['appstream']) && ($this->data['appstream'] != false);
        }
        function AppStreamId() {
            return $this->data['appstream'];
        }

        function hasKDEApps() {
            return $this->data['kde-apps.org'] != false;
        }
        function KDEAppsId() {
            return $this->data['kde-apps.org'];
        }

        function hasUserbase() {
            return $this->data['userbase'] != false;
        }
        function userbase() {
            return "http://userbase.kde.org/".$this->data['userbase'];
        }

        function hasHandbook() {
            return $this->data['handbook'] != false;
        }
        function handbook() {
            return $this->data['handbook'];
        }

        function forumUrl() {
            if ($this->data['forum'] != false) {
                return 'http://forum.kde.org/viewforum.php?f='.$this->data['forum'];
            } else {
                return 'http://forum.kde.org/';
            }
        }

        function ircChannels() {
            if ($this->data['irc'] != false) {
                $irc = $this->data['irc'];
                if (is_array($irc)) {
                    return $irc;
                } else {
                    return array($irc);
                }
            } else {
                return array("#kde");
            }
        }

        function mailingLists() {
            if ($this->data['mailing lists'] != false) {
                $ml = $this->data['mailing lists'];
                if (is_array($ml)) {
                    return $ml;
                } else {
                    return array($ml);
                }
            } else {
                return array("kde@kde.org");
            }
        }

        function browseSourcesHtml() {
            if ($this->data['repository'][0] == 'svn') {
                return '<p><a href="http://websvn.kde.org/'.$this->data['repository'][1].'">'.i18n_var("Browse %1 source code on WebSVN", $this->name()).'</a></p>';
            } else if($this->data['repository'][0] == 'git') {
                $path = '<p><a href="https://commits.kde.org/'.$this->data['repository'][1];
                $path .= '?path=/">'.i18n_var("Browse %1 source code online", $this->name()).'</a></p>';
                return $path;
            }
            return '';
        }

        function checkoutSourcesHtml() {
            if ($this->data['repository'][0] == 'svn') {
                return '<p>'.i18n_var("Checkout source code:", $this->name()).'</p><p><code>svn co svn://anonsvn.kde.org/home/kde/'.$this->data['repository'][1].'</code></p>';
            } else if($this->data['repository'][0] == 'git') {
                return '<p>'.i18n_var("Clone %1 source code:", $this->name()).'</p><p><code>git clone git://anongit.kde.org/'.$this->data['repository'][1].'</code></p>';
            }
            return '';
        }

        function hasCi() {
            return $this->data['ci'] != false;
        }

        function ciUrl() {
            if (is_array($this->data['ci'])) {
                $group = $this->data['ci'][0];
                $repository = $this->data['ci'][1];
            } else {
                $group = $this->data['ci'];
                $repository = $this->data['repository'][1];
            }
            return "https://build.kde.org/job/".$group."/job/".$repository."/";
        }

        //Returns true if the application has a bugtracker (external or KDE bugzilla)
        function hasBugTracker() {
            return $this->data['bugs'] != false;
        }

        //Returns true if the application has an external bugtracker (!bugs.k.o)
        function isBugTrackerExternal() {
            return (substr($this->data['bugs'], 0, 7) == "http://" ||
                substr($this->data['bugs'], 0, 8) == "https://");
        }

        function bugzillaProduct() {
            if (is_array($this->data['bugs'])) {
                return $this->data['bugs'][0];
            }
            return $this->data['bugs'];
        }

        function bugzillaComponent() {
            if (is_array($this->data['bugs'])) {
                return $this->data['bugs'][1];
            }
            return false;
        }

        function hasEbn() {
            return true;
        }

        function ebnUrlBase() {
            // examples:
            // KDE/kdeedu/parley:               http://englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdeedu/parley/index.html
            // KDE/kdebase/apps/dolphin:        http://englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdebase-apps/dolphin/index.html
            // extragear/graphics/digikam:      http://englishbreakfastnetwork.org/krazy/reports/extragear/graphics/digikam/index.html
            // koffice/kword:                   http://englishbreakfastnetwork.org/krazy/reports/bundled-apps/koffice/kword/index.html
            // amarok (gitorious):              http://englishbreakfastnetwork.org/krazy/reports/extragear/multimedia/amarok-git/index.html

            $ebn = '';
            if ($this->data['repository'][0] == 'svn') {
                $parts = explode ('/', $this->data['repository'][1]);
                $len = count($parts);
                if (!strlen($parts[$len-1])) {
                    array_pop($parts);
                    $len--;
                }
                if (in_array('KDE', $parts)) {
                    if ($parts[$len-3] == 'kdebase') {
                        $ebn = 'kde-4.x/kdebase-'.$parts[$len-2].'/'.$parts[$len-1];
                    } else {
                        $ebn = 'kde-4.x/'.$parts[$len-2].'/'.$parts[$len-1];
                    }
                } else if (in_array('extragear', $parts)) {
                    $ebn = 'extragear/'.$parts[$len-2].'/'.$parts[$len-1];
                } else if (in_array('koffice', $parts)) {
                    $ebn = 'bundled-apps/'.$parts[$len-2].'/'.$parts[$len-1];
                }
            } else if($this->data['repository'][0] == 'git') {
                // extract path segments from module parent
                $parts = explode('/', $this->data['parent']);

                // replace path segment by "kde-4.x" for "kde"
                if ($parts[0] == 'kde') {
                    $ebn = "kde-4.x/" . $parts[1] . "/" . strtolower($this->data['repository'][1]);
		} else {
                    $ebn = $parts[0] . "/" . $parts[1] . "/" . strtolower($this->name());  
		}
		return $ebn;
	    }
        }

        function ebnCodeCheckingUrl()
        {
            return 'http://ebn.kde.org/krazy/reports/'.$this->ebnUrlBase().'/index.html';
        }

        function ebnDocCheckingUrl() 
        {
            return 'http://ebn.kde.org/sanitizer/reports/'.$this->ebnUrlBase().'/index.html';
        }
}
