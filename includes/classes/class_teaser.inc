<?php
/**
* Class to populate teaser images
* Written by Sayak Banerjee <sayakb@kde.org>
*
* The class now looks for teaser filenames and information in
* $document_root/images/teaser/teaser.json
* If this file does not exist it scans this directory for all image files
* Json Extension written by Matthias Meßmer <matthias@familie-messmer.de> 
* 2010/06
*/

class teaserImageBuilder
{
	var $fileData; // containing alt and href information
	var $files = array(); // containing filenames
	var $options = array(); // slideshow options
	
	//
	// Constructor, setting up the arrays
	//	
	function teaserImageBuilder()
	{
		$this->fileData = $this->getJsonData(); // maybe empty
		$this->files = $this->getFiles();
	}
	
	//
	// Function to generate the image list
	//
	function generate()
	{
		$flag = false;	
	
		// Start away
		echo "\n<span class=\"teaser\">\n";
		
		// Get a list of files
		foreach ($this->files as $file)
		{
			// Reset strings 
			$alt = "";
			$href = "";			
			
			if ($this->fileData && $this->fileData[$file] )
			{
				// Get info from the array if available
				if (isset($this->fileData[$file]["alt"])) $alt = $this->fileData[$file]["alt"];
				if (isset($this->fileData[$file]["href"])) $href = $this->fileData[$file]["href"];
			}
			
			// Img tag string
			$imgTag = "<img " . ($flag ? "class=\"teaser_hide\" " : "") . "src=\"/images/teaser/{$file}\" alt=\"$alt\" />";
			
			// Do we have a link?			
			if (!empty($href))
			{
				// Enclose the img tag with an a tag
				echo "\t<a href=\"{$href}\">{$imgTag}</a>\n";
			}
			else
			{
				echo "\t{$imgTag}\n";
			}
			
			if (!$flag)
			{
				// Set the flag
				$flag = true;
			}
		}
	
		// That's it!
		echo "</span>\n";
	}
	
	//
	// Function to print a raw image list
	//
	function imageList()
	{
		$out = "";
	
		foreach ($this->files as $file)
		{
			$out .= "\"/images/teaser/{$file}\",";
		}
		
		// Trim the last comma
		$out = substr($out, 0, strlen($out) - 1);
		
		// Output the list
		echo $out;
	}
	
	//
	// Function to return a file list
	//
	function getFiles()
	{ 
		global $document_root;
		
		// Additional Data available?
		if( $this->fileData )
		{
			// Filenames are the keys of the top level array			
			$fileList = array_keys($this->fileData);
		}
		else // Fallback to scnadir
		{
			// Get a list of files
			foreach (array_diff(scandir("{$document_root}/images/teaser"), array('.','..')) as $file)
			{
				// If it is a jpg file
				if (is_file("{$document_root}/images/teaser/{$file}") && ereg('jpg|jpeg|png|gif$', $file))
				{
					// Append the file to the array
					$fileList[] = $file; 
				}
			}
		}
		
		// Return the file list
		return $fileList; 
	}
	
	//
	// Get filenames, href and alt information from teaser.json
	// Also set the various teaser slideshow options
	//
	function getJsonData()
	{
		global $document_root;
		
		// Look for teaser.json
		if (file_exists($document_root . "/images/teaser/teaser.json"))
		{
			// Parse json file			
			$values = json_decode(file_get_contents($document_root . "/images/teaser/teaser.json"), true);
			
			// Set the options
			$this->options = $values["teaserOptions"];
			
			// Return file data
			return $values["teaserImages"];
		}
		else
		{
			return false;
		}
	}
}

?>