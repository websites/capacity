<?php

include("classes/class_handler404.inc");

$handler = new Handler404();

$handler->add("/anoncvs.html", "http://techbase.kde.org/Getting_Started/Sources/Anonymous_SVN");
$handler->add("/applications.html", "http://kde-apps.org/");
$handler->add("/family.html", "/family/");
$handler->add("/contact.html", "/contact/");
$handler->add("/awards.html", "/awards/");
$handler->add("/bulletin", "/whatiskde/project.php");
$handler->add("/bulletin/index.html", "/whatiskde/project.php");
$handler->add("/helping.html", "/support/");
$handler->add("/helping", "/support/");
$handler->add("/helping/index.php", "/support/");
$handler->add("/faq.html", "/info/faq.php");
$handler->add("/cdrom.html", "/download/cdrom.php");
$handler->add("/cvsup.html", "http://techbase.kde.org/Getting_Started/Sources/Anonymous_SVN");
$handler->add("/compilationfaq.html", "http://techbase.kde.org/Getting_Started/Build/FAQ");
$handler->add("/comments.html", "/whatiskde/comments.php");
$handler->add("/cvsupmirrors.html", "http://techbase.kde.org/Getting_Started/Sources/Anonymous_SVN");
$handler->add("/credits.html", "/people/credits/");
$handler->add("/people/credits.php", "/people/credits/");
$handler->add("/developers.html", "http://techbase.kde.org/");
$handler->add("/description.html", "/whatiskde/");
$handler->add("/description2.html", "/whatiskde/");
$handler->add("/download.html", "/download/");
$handler->add("/download/stable", "http://download.kde.org/stable/");
$handler->add("/packagepolicy.html", "/download/packagepolicy.php");
$handler->add("/signature.html", "/download/signature.php");
$handler->add("/kdeships.html", "/download/distributions.php");
$handler->add("/donations.html", "/support/donations.php");
$handler->add("/support.html", "/support/");
$handler->add("/thanks.html", "/support/thanks.php");
$handler->add("/food/food.html", "/food/");
$handler->add("/ftpmirror.info", "/mirrors/ftp.info");
$handler->add("/webmirrors.html", "/mirrors/web.php");
$handler->add("/webmirrors_howto.html", "/mirrors/web_howto.php");
$handler->add("/mailinglists.html", "/mailinglists/");
$handler->add("/mirrors.html", "/mirrors/");
$handler->add("/news_dyn.html", "http://dot.kde.org/");
$handler->add("/kde-manifesto.html", "/whatiskde/kdemanifesto.php");
$handler->add("/kde1-and-kde2.html", "http://developer.kde.org/build/kde1-and-kde2.html");
$handler->add("/kde2-and-kde3.html", "http://developer.kde.org/build/kde2-and-kde3.html");
$handler->add("/kdeqtfoundation.html", "/whatiskde/kdefreeqtfoundation.php");
$handler->add("/kde_print.html", "http://printing.kde.org/");
$handler->add("/konqueror", "http://konqueror.kde.org/");
$handler->add("/documentation/faq", "http://docs.kde.org/development/en/kdebase-runtime/faq/");
$handler->add("/documentation/faq/index.html", "http://docs.kde.org/development/en/kdebase-runtime/faq/");
$handler->add("/documentation/faq/install.html", "http://docs.kde.org/development/en/kdebase-runtime/faq/install.html");
$handler->add("/events-1998.html", "http://events.kde.org/calendar/events-1998.php");
$handler->add("/events-1999.html", "http://events.kde.org/calendar/events-1999.php");
$handler->add("/events-2000.html", "http://events.kde.org/calendar/events-2000.php");
$handler->add("/events-2001.html", "http://events.kde.org/calendar/events-2001.php");
$handler->add("/events-2002.html", "http://events.kde.org/calendar/events-2002.php");
$handler->add("/events.html", "http://events.kde.org/calendar/coming.php");
$handler->add("/ftpmirrors.html", "/mirrors/ftp.php");
$handler->add("/ftpmirrors_howto.html", "/mirrors/ftp_howto.php");
$handler->add("/gallery/index.html", "/people/gallery.php");
$handler->add("/gallery", "/people/gallery.php");
$handler->add("/whatiskde/BSD", "/whatiskde/licenses/BSD");
$handler->add("/whatiskde/COPYING", "/whatiskde/licenses/COPYING");
$handler->add("/whatiskde/COPYING.LIB", "/whatiskde/licenses/COPYING.LIB");
$handler->add("/whatiskde/LICENSE", "/whatiskde/licenses/LICENSE");
$handler->add("/whatiskde/proj.html", "/whatiskde/project.php");
$handler->add("/howto-i18n.html", "http://i18n.kde.org/");
$handler->add("/contact/impressum.php", "http://www.kde.org/community/whatiskde/impressum.php");
$handler->add("/i18n.html", "http://i18n.kde.org/");
$handler->add("/install-source.html", "http://techbase.kde.org/Getting_Started");
$handler->add("/install-problems.html", "/info/releases.php");
$handler->add("/install-binaries.html", "/documentation/faq/install.html");
$handler->add("/international/czechia", "http://czechia.kde.org/");
$handler->add("/info/faq.html", "/info/faq-2.x.php");
$handler->add("/info/overview.html", "/info/");
$handler->add("/jobs/index.html", "/jobs/");
$handler->add("/jobs.html", "/jobs/");
$handler->add("/jobs/jobs-closed.html", "/jobs/");
$handler->add("/jobs/jobs-open.html", "/jobs/open.php");
$handler->add("/jobs/jobs-taken.html", "/jobs/open.php");
$handler->add("/kate/hl/update.xml", "/apps/kate/hl/update.xml");
$handler->add("/kde2shots.html", "/screenshots/kde2shots.php");
$handler->add("/kscreenshots.html", "/screenshots/");
$handler->add("/klock_alert.html", "/info/security/advisory-19981118-1.txt");
$handler->add("/kpilot", "http://pim.kde.org/components/kpilot.php");
$handler->add("/themes", "http://www.kde-look.org/");
$handler->add("/announcements/changelog1_0to1_1.html", "/announcements/changelogs/changelog1_0to1_1.php");
$handler->add("/announcements/changelog1_1to1_1_1.html", "/announcements/changelogs/changelog1_1to1_1_1.php");
$handler->add("/announcements/changelog1_1_1to1_1_2.html", "/announcements/changelogs/changelog1_1_1to1_1_2.php");
$handler->add("/announcements/changelog2_0to2_0_1.html", "/announcements/changelogs/changelog2_0to2_0_1.php");
$handler->add("/announcements/changelog2_0to2_1.html", "/announcements/changelogs/changelog2_0to2_1.php");
$handler->add("/announcements/changelog2_1to2_1_1.html", "/announcements/changelogs/changelog2_1to2_1_1.php");
$handler->add("/announcements/changelog2_1_1to2_1_2.html", "/announcements/changelogs/changelog2_1_1to2_1_2.php");
$handler->add("/announcements/changelog2_1to2_2.html", "/announcements/changelogs/changelog2_1to2_2.php");
$handler->add("/announcements/changelog2_2to2_2_1.html", "/announcements/changelogs/changelog2_2to2_2_1.php");
$handler->add("/announcements/changelog2_2_1to2_2_2.html", "/announcements/changelogs/changelog2_2_1to2_2_2.php");
$handler->add("/announcements/changelog2_2_2to3_0.html", "/announcements/changelogs/changelog2_2_2to3_0.php");
$handler->add("/announcements/changelog3_0to3_0_1.html", "/announcements/changelogs/changelog3_0to3_0_1.php");
$handler->add("/announcements/changelog3_0_1to3_0_2.html", "/announcements/changelogs/changelog3_0_1to3_0_2.php");
$handler->add("/people.html", "/people/");
$handler->add("/people/people.html", "http://people.kde.org/");
$handler->add("/pim", "http://pim.kde.org/");
$handler->add("/release.html", "http://techbase.kde.org/Schedules/Release_Schedules_Guide");
$handler->add("/representatives.html", "/contact/");
$handler->add("/requirements.html", "/info/requirements/");
$handler->add("/screenshots/kde3shots.html", "/screenshots/kde300shots.php");
$handler->add("/screenshots/kde300beta2shots.html", "/screenshots/kde300shots.php");
$handler->add("/screenshots/mini/kde2final_3.jpg", "/screenshots/images/mini/kde2final_3.jpg");
$handler->add("/screenshots/large/kde3-snapshot1.jpg", "/screenshots/images/large/kde3-snapshot1.jpg");
$handler->add("/screenshots/large/kde2final_3.jpg", "/screenshots/images/large/kde2final_3.jpg");
$handler->add("/solariscompile.txt", "http://developer.kde.org/build/solariscompile.html");
$handler->add("/misc.html", "/stuff/");
$handler->add("/kde-stuff.html", "/stuff/");
$handler->add("/kde-books.html", "/stuff/books.php");
$handler->add("/kde-press.html", "/stuff/press.php");
$handler->add("/clipart.html", "/stuff/clipart.php");
$handler->add("/technology.html", "/history/kde-two-report.php");
$handler->add("/kde-year2000.html", "/info/year2000.php");
$handler->add("/kde-press-1998.html", "/stuff/press.php");
$handler->add("/kde-press-1999.html", "/stuff/press.php");
$handler->add("/kde-one.html", "/history/kde-one.php");
$handler->add("/kde-two.html", "/history/kde-two.php");
$handler->add("/kde-two", "/history/kde-two.php");
$handler->add("/announcements/kde-three-report.php", "/history/kde-three.php");
$handler->add("/announce/k3b-announce.php", "/history/kde-threeb.php");
$handler->add("/announce/k3c-announce.php", "/history/kde-threeb1.php");
$handler->add("/kdebuttons/kdebuttons.html", "/stuff/clipart.php");
$handler->add("/thanks_pp.html", "/support/thanks_pp.html");
$handler->add("/whatiskde/join.php", "http://www.kde.org/support/");
$handler->addDir("/users/", "http://userbase.kde.org/");
$handler->add("/users/faq.php", "http://userbase.kde.org/General_KDE_FAQs");
$handler->add("/users/glossary.php", "http://userbase.kde.org/Glossary");
$handler->add("/contact/impressum-en.php", "/community/whatiskde/impressum-en.php");
$handler->addDir("/contact/", "/contact/representatives.php");
$handler->addDir("/mailinglists/", "/support/mailinglists/");
$handler->addDir("/jobs/", "/community/getinvolved/");
$handler->add("/jobs", "/community/getinvolved/");
$handler->addDir("/getinvolved/", "/community/getinvolved/");
$handler->addDir("/userpictures/", "/community/"); //no need to request an image dir, so let's redirect to community
$handler->add("/jj", "https://bugs.kde.org/buglist.cgi?keywords=junior-jobs&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit");
//
// redirect whole subdirs, will be tried last ;)
//

// old areas/
// missing: appmonth, cvs-digest, guidelines
$handler->addDir("/areas/accessibility", "http://accessibility.kde.org");
$handler->addDir("/areas/docs", "http://docs.kde.org");
$handler->addDir("/areas/download", "http://download.kde.org");
$handler->addDir("/areas/edu", "http://edu.kde.org");
$handler->addDir("/areas/enterprise", "http://enterprise.kde.org");
$handler->addDir("/areas/events", "http://events.kde.org");
$handler->addDir("/areas/extragear", "http://extragear.kde.org");
$handler->addDir("/areas/games", "http://games.kde.org");
$handler->addDir("/areas/kde-ev", "http://ev.kde.org");
$handler->addDir("/areas/kdemail", "http://kdemail.net");
$handler->addDir("/areas/kdetalk", "http://kdetalk.net");
$handler->addDir("/areas/koffice", "http://www.koffice.org");
$handler->addDir("/areas/l10n", "http://l10n.kde.org");
$handler->addDir("/areas/multimedia", "http://multimedia.kde.org");
$handler->addDir("/areas/people", "http://people.kde.org");
$handler->addDir("/areas/pim", "http://pim.kde.org");
$handler->addDir("/areas/printing", "http://printing.kde.org");
$handler->addDir("/areas/quality", "http://quality.kde.org");
$handler->addDir("/areas/solaris", "http://solaris.kde.org");
$handler->addDir("/areas/usability", "http://usability.kde.org");
$handler->addDir("/areas/women", "http://women.kde.org");
$handler->addDir("/areas/worldwide", "http://worldwide.kde.org");
$handler->addDir("/areas/sysadmin", "http://techbase.kde.org/SysAdmin");
$handler->addDir("/applications/education", "http://edu.kde.org/applications");

// old apps/ 
// missing: kooka
$handler->addDir("/apps/cervisia", "http://cervisia.kde.org");
$handler->addDir("/apps/kafka", "http://kafka.kde.org");
$handler->addDir("/apps/kate", "http://kate.kde.org");
$handler->addDir("/apps/kmplayer", "http://kmplayer.kde.org");
$handler->addDir("/apps/konqueror", "http://www.konqueror.org");
$handler->addDir("/apps/konsole", "http://konsole.kde.org");
$handler->addDir("/apps/kontact", "http://kontact.kde.org");
$handler->addDir("/apps/kopete", "http://kopete.kde.org");
$handler->addDir("/apps/kpdf", "http://kpdf.kde.org");
$handler->addDir("/apps/ksvg", "http://svg.kde.org");
$handler->addDir("/apps/noatun", "http://noatun.kde.org");

// old international pages
$handler->addDir("/es", "http://es.kde.org");
$handler->addDir("/fr", "http://fr.kde.org");
$handler->addDir("/il", "http://il.kde.org");

// misc very old stuff
$handler->addDir("/artist", "http://artist.kde.org");
$handler->addDir("/extragear", "http://extragear.kde.org");
$handler->addDir("/kde-ev", "http://ev.kde.org");
$handler->addDir("/kmail", "http://kmail.kde.org");
$handler->addDir("/kontact", "http://kontact.kde.org");
$handler->addDir("/kopete", "http://kopete.kde.org");
$handler->addDir("/people", "http://people.kde.org");

// Content largely moved around...
$handler->addDir("/getinvolved", "http://www.kde.org/community/getinvolved");
$handler->addDir("/history", "http://www.kde.org/community/history");

//$handler->mail("ingomalchow@googlemail.com");

// do it ;)
$handler->execute();

?>
