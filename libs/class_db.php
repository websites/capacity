<?php


/**
* DB Lib
*
* @author Frank Karlitschek 
* @copyright 2010 Frank Karlitschek karlitschek@kde.org 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
* License as published by the Free Software Foundation; either 
* version 3 of the License, or any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU AFFERO GENERAL PUBLIC LICENSE for more details.
*  
* You should have received a copy of the GNU Lesser General Public 
* License along with this library.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

$path = '/srv/www/capacity-auth';
set_include_path(get_include_path() . PATH_SEPARATOR . $path . PATH_SEPARATOR . "media/");
require_once('db_auth.inc');


// set the default timezone to use. Available since PHP 5.1
date_default_timezone_set('Europe/Berlin');


class DB {

  // DB config
  const DB_STAGE   = true;


  /**
   * executes a query on the database
   *
   * @param string $cmd
   * @return result-set
   */
  static function query($cmd) {
    global $db_username;
    global $db_password;
    global $db_database;
    global $DBConnection;
    if(!isset($DBConnection)) {
      $DBConnection = @new mysqli('localhost', $db_username, $db_password,$db_database);
      if (mysqli_connect_errno()) {
        @ob_end_clean();
        echo('<html><head></head><body bgcolor="#F0F0F0"><br /><br /><center><b>We are down for maintenance.</b><br /><br />Sorry for the inconvenience.<br /> We\'ll be back up as soon as 
possible. Please try again later.</center></body></html>');
        exit();
      }
    }
    $result = @$DBConnection->query($cmd);
    if (!$result) {
      $entry='DB Error: "'.$DBConnection->error.'"<br />';
      $entry.='Offending command was: '.$cmd.'<br />';
      if(DB_STAGE) echo($entry);
      error_log($entry);
    }
    return $result;
  }

  /**
   * closing a db connection
   *
   * @return bool
   */
  static function close() {
    global $DBConnection;
    if(isset($DBConnection)) {
      return $DBConnection->close();
    } else {
      return(false);
    }
  }

  /**
   * Returning primarykey if last statement was an insert.
   *
   * @return primarykey
   */
  static function insertid() {
    global $DBConnection;
    return(mysqli_insert_id($DBConnection));
  }

  /**
   * Returning number of rows in a result
   *
   * @param resultset $result
   * @return int
   */
  static function numrows($result) {
    if(!isset($result) or ($result == false)) return 0;
    $num= mysqli_num_rows($result);
    return($num);
  }

  /**
   * Returning number of affected rows
   *
   * @return int
   */
  static function affected_rows() {
    global $DBConnection;
    if(!isset($DBConnection) or ($DBConnection==false)) return 0;
    $num= mysqli_affected_rows($DBConnection);
    return($num);
  }


  /**
   * get a field from the resultset
   *
   * @param resultset $result
   * @param int $i
   * @param int $field
   * @return unknown
   */
  static function result($result, $i, $field) {
    //return @mysqli_result($result, $i, $field);

    mysqli_data_seek($result,$i);
    if (is_string($field))
    $tmp=mysqli_fetch_array($result,MYSQLI_BOTH);
    else
    $tmp=mysqli_fetch_array($result,MYSQLI_NUM);
    $tmp=$tmp[$field];
    return($tmp);

  }

  /**
   * get data-array from resultset
   *
   * @param resultset $result
   * @return data
   */
  static function fetch_assoc($result) {
    return mysqli_fetch_assoc($result);
  }


  /**
   * Freeing resultset (performance)
   *
   * @param unknown_type $result
   * @return bool
   */
  static function free_result($result) {
    return @mysqli_free_result($result);
  }



}


?>
