$(document).ready(function() {
    let piwikCookieBanner = {
        setCookie: function(cname, cvalue) {
            let hostParts = window.location.host.split(".")
            let wildcardDomain = hostParts.pop();
            wildcardDomain = `.${hostParts.pop()}.${wildcardDomain}`;

            let d = new Date();
            d.setTime(d.getTime() + (365*24*60*60*1000));
            let expires = "expires="+d.toUTCString();
            document.cookie = `${cname}=${cvalue}; ${expires}; path=/; domain=${wildcardDomain}`;
        },

        getCookie: function(cname) {
            let name = cname + "=";
            let ca = document.cookie.split(';');
            for(let i=0; i<ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return "";
        },

        getOptOutIframe: function(language) {
            let text = '';
            if(language == 0) {
                $('.teaser').css('display', 'none');

                text = `<p>Our website uses piwik, a so called web analyzer service. Piwik uses "cookies", those are text files stored on your computer and enabling us to analyse the usage of the website. For this purpose usage statistics generated from the cookies (including your shortened ip address) are sent to our server and saved for usage analytics, to optimize our websites. During this process your ip address immediately gets anonymized so you as user are completely anonymous to us. The generated information by the cookies are not passed on to a third party. You can prevent the usage of cookies by a corresponding setting in your browser software, but in this case you might not be able to use this website with full functionality.</p>
                        <p>If you do not agree with the storage and evaluation of your data from your visit, then you can opt-out of storage and usage via a mouse click. In this case your browser will store a so called Opt-Out-Cookie, which tells Piwik to not analyse any session data. Attention: If you delete all your cookies, your Opt-Out-Cookie will be deleted as well, in which case you probably need to activate it again.</p>`;

                if(navigator.language == 'de') {
                    text = `<p>Unsere Website verwendet Piwik, dabei handelt es sich um einen sogenannten Webanalysedienst. Piwik verwendet sog. „Cookies“, das sind Textdateien, die auf Ihrem Computer gespeichert werden und die unsererseits eine Analyse der Benutzung der Webseite ermöglichen. Zu diesem Zweck werden die durch den Cookie erzeugten Nutzungsinformationen (einschließlich Ihrer gekürzten IP-Adresse) an unseren Server übertragen und zu Nutzungsanalysezwecken gespeichert, was der Webseitenoptimierung unsererseits dient. Ihre IP-Adresse wird bei diesem Vorgang umge­hend anony­mi­siert, so dass Sie als Nutzer für uns anonym bleiben. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Webseite werden nicht an Dritte weitergegeben. Sie können die Verwendung der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern, es kann jedoch sein, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können.</p>
                        <p>Wenn Sie mit der Spei­che­rung und Aus­wer­tung die­ser Daten aus Ihrem Besuch nicht ein­ver­stan­den sind, dann kön­nen Sie der Spei­che­rung und Nut­zung nachfolgend per Maus­klick jederzeit wider­spre­chen. In diesem Fall wird in Ihrem Browser ein sog. Opt-Out-Cookie abgelegt, was zur Folge hat, dass Piwik kei­ner­lei Sit­zungs­da­ten erhebt. Achtung: Wenn Sie Ihre Cookies löschen, so hat dies zur Folge, dass auch das Opt-Out-Cookie gelöscht wird und ggf. von Ihnen erneut aktiviert werden muss.</p>`;
                }

                $('#main').html(`
                    ${text}
                    <iframe width="100%" src="https://stats.kde.org/index.php?module=CoreAdminHome&action=optOut&language=${navigator.language}" /><p><a href="${window.location.protocol}//${window.location.host}">Return to content</a></p>
                    <p>We are detecting your language from your browser settings:</p>
                    <p><a class="optoutenglish" href="#">See this text in English</a></p>
                    <p><a class="optoutgerman" href="#">See this text in German</a></p>`);
            }
            if(language == 1) {
                $('.teaser').css('display', 'none');
                $('#main').html(`
                    <p>Our website uses piwik, a so called web analyzer service. Piwik uses "cookies", those are text files stored on your computer and enabling us to analyse the usage of the website. For this purpose usage statistics generated from the cookies (including your shortened ip address) are sent to our server and saved for usage analytics, to optimize our websites. During this process your ip address immediately gets anonymized so you as user are completely anonymous to us. The generated information by the cookies are not passed on to a third party. You can prevent the usage of cookies by a corresponding setting in your browser software, but in this case you might not be able to use this website with full functionality.</p>
                    <p>If you do not agree with the storage and evaluation of your data from your visit, then you can opt-out of storage and usage via a mouse click. In this case your browser will store a so called Opt-Out-Cookie, which tells Piwik to not analyse any session data. Attention: If you delete all your cookies, your Opt-Out-Cookie will be deleted as well, in which case you probably need to activate it again.</p>
                    <iframe width="100%" src="https://stats.kde.org/index.php?module=CoreAdminHome&action=optOut&language=en" /><p><a href="${window.location.protocol}//${window.location.host}">Return to content</a></p>
                    <p><a class="optoutgerman" href="#">See this text in German</a></p>`);
            }
            if(language == 2) {
                $('.teaser').css('display', 'none');
                $('#main').html(`
                    <p>Unsere Website verwendet Piwik, dabei handelt es sich um einen sogenannten Webanalysedienst. Piwik verwendet sog. „Cookies“, das sind Textdateien, die auf Ihrem Computer gespeichert werden und die unsererseits eine Analyse der Benutzung der Webseite ermöglichen. Zu diesem Zweck werden die durch den Cookie erzeugten Nutzungsinformationen (einschließlich Ihrer gekürzten IP-Adresse) an unseren Server übertragen und zu Nutzungsanalysezwecken gespeichert, was der Webseitenoptimierung unsererseits dient. Ihre IP-Adresse wird bei diesem Vorgang umge­hend anony­mi­siert, so dass Sie als Nutzer für uns anonym bleiben. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Webseite werden nicht an Dritte weitergegeben. Sie können die Verwendung der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern, es kann jedoch sein, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können.</p>
                        <p>Wenn Sie mit der Spei­che­rung und Aus­wer­tung die­ser Daten aus Ihrem Besuch nicht ein­ver­stan­den sind, dann kön­nen Sie der Spei­che­rung und Nut­zung nachfolgend per Maus­klick jederzeit wider­spre­chen. In diesem Fall wird in Ihrem Browser ein sog. Opt-Out-Cookie abgelegt, was zur Folge hat, dass Piwik kei­ner­lei Sit­zungs­da­ten erhebt. Achtung: Wenn Sie Ihre Cookies löschen, so hat dies zur Folge, dass auch das Opt-Out-Cookie gelöscht wird und ggf. von Ihnen erneut aktiviert werden muss.</p>
                    <iframe width="100%" src="https://stats.kde.org/index.php?module=CoreAdminHome&action=optOut&language=de" /><p><a href="${window.location.protocol}//${window.location.host}">Zurück zum Inhalt</a></p>
                    <p><a class="optoutenglish" href="#">See this text in English</a></p>`);
            }

            $('.optoutenglish').click(function(e) {
                e.preventDefault();
                piwikCookieBanner.getOptOutIframe(1);
            });

            $('.optoutgerman').click(function(e) {
                e.preventDefault();
                piwikCookieBanner.getOptOutIframe(2);
            });

        },

        addBannerMarkup: function() {
            $('body').prepend(`<style type="text/css">
                #cookiebanner {
                    display: flex;
                    text-align: left;
                    background-color: #E3F4F8;
                    border-bottom: solid #657187 3px;
                    padding: 10px;
                    justify-content: space-between;
                }

                #dismisscookiebanner {
                    padding: 5px;
                    min-width: 120px;
                    align-self: center;
                    margin-left: 5px;
                }
                </style>

                <div id="cookiebanner" class="cookiebanner">
                    <div>KDE uses cookies on our website to help make it better for you. If you would like to learn more, or opt out of this feature: <a href="#" class="optoutcookebanner">click here</a></div>
                    <button class="btn button" id="dismisscookiebanner">I Understand</button>
                </div>`);
            }
        }

    if(piwikCookieBanner.getCookie('cookiebannerdismissed') !== "kde") {
        piwikCookieBanner.addBannerMarkup();
    }

    $('#dismisscookiebanner').click(function() {
        piwikCookieBanner.setCookie('cookiebannerdismissed', 'kde');
        $('#cookiebanner').css('display', 'none');
    });

    $('.optoutcookebanner').click(function(e) {
        e.preventDefault();
        piwikCookieBanner.getOptOutIframe(0);
    });
});
