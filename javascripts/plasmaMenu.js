/**
*
* @package menu_kde_org
* @version $Id: main.js sayakb $
* @copyright (c) 2010 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/** 
* Global variables
*/
// Array holding category names
var categories = new Array();

// Placeholder to save current active tab
var activeTab = null;

// TimeOut event placeholder
var hoverEvent = null;

// Internet Explorer switch
var isIe = false;

// Flag to check if a menu is sliding
var menuOpening = false;

// Container to indicate open submenu
var openSub = null;

// Submenu open hover event
var openSubEve = null;

/**
* Function to detect the web browser
*/
function detectBrowser()
{
	// Does the browser have 'Microsoft' in it's name?
	if (navigator.appName.indexOf('Microsoft') != -1)
	{
		// Yes, the user is using IE
		isIe = true;
	}
}

/**
* Lets prepare the manu!
*/
function prepareMenu()
{
	// Get all the divs
	var catDivs = document.getElementsByTagName('div');
	var catStr = '';
	
	// Recurse through all divs
	for (var cIdx = 0; cIdx < catDivs.length; cIdx++)
	{
		// Detect whether the div is a category
		if (catDivs[cIdx].id.substr(0, 9) == 'mainmenu_')
		{	
			// Concatinate to a string
			catStr += catDivs[cIdx].id.replace('mainmenu_', '') + ',';
		}
	}
	
	// Trim the last comma
	catStr = catStr.substr(0, catStr.length - 1);
	
	// Make an array out of it
	categories = catStr.split(',');

	for (var idx = 0; idx < categories.length; idx++)
	{
		// Go through all categories and hide all their menus
		$('.mainmenu_' + categories[idx] + '_sub').css({
			display: 'none'
		});
	}
	
	// This is done to disable the CSS :hover trigger
	$('div').removeClass('plasmamenu_box_submenu');
	$('li').removeClass('menubox_entry');
	$('.menubox_subhover').addClass('menubox_subhover_js');
	$('.menubox_subhover_js').removeClass('menubox_subhover');
	
	// Setup click functions
	$('div').click(function() {
		if ($(this).attr('id').substr(0, 9) == 'mainmenu_')
		{
			// Show the menu
			showMenu($(this).attr('class'));
		}
	});
	
	// Setup hover functions
	$('div').mouseover(function() {
		if ($(this).attr('id').substr(0, 9) == 'mainmenu_')
		{
			// Slide an open submenu
			slideMenu($(this).attr('class'));
		}
	});
	
	// Submenu hover events
	$('.menubox_subhover_js').mouseover(function() {
		// Take the class name that was hovered, trim all spaces
		var divClass = 
			$(this).attr('class').
			replace('index', '').
			replace('menubox_subhover_js', '').
			replace(/^\s*/, "").
			replace(/\s*$/, "")
			+ '_sub';
			
		// Show the submenu
		clearTimeout(openSubEve);
		openSubEve = setTimeout(function() {
			showSubMenu(divClass);
		}, 202);
	});
	
	// Event to hide a submenu
	$('li').mouseover(function() {
		// When hovering a non submenu item or it's parent
		if ($(this).attr('class').indexOf('index') == 0 &&
			$(this).attr('class').indexOf(openSub) == -1)
		{
			if ($(this).attr('class').indexOf('menubox_subhover_js') == -1)
			{
				// Clear the open event
				clearTimeout(openSubEve);
			}
		
			// Hide all open submenus
			hideSubMenu();
		}
	});
	
	// Check for the escape key to close the menu
	$(document).keyup(function(event) {
		if (event.keyCode == 27)
		{
			hideMenu();
		}
	});
}

/** 
* Show the menu
*/
function showMenu(divClass)
{
	// Unset isShown at startup
	var isShown = false;

	for (var idx = 0; idx < categories.length; idx++)
	{
		// Is the menu already shown?
		if ($('.mainmenu_' + categories[idx] + '_sub').css('display') != 'none')
		{
			isShown = true;
		}
	}
	
	if (!isShown)
	{	
		// Set menu opening flag
		menuOpening = true;
	
		// Set the active tag
		activeTab = divClass;
		
		// Set the class name
		var className = '.' + divClass + '_sub';
		
		// Calculate the position to slide from
		var newPos = $(className).position().top - 40;
	
		// Add a floating div
		$('body').append("<div id=\"plasmamenu_temp\"></div>");
		$('#plasmamenu_temp').css({
			position: 'absolute',
			height: $('body').height().toString() + 'px',
			width: $('body').width().toString() + 'px',
			zIndex: '9999',
			top: '0px',
			left: '0px',
			cursor: 'pointer'
		});

		// Set new coordinates temporarily
		$(className).css({
			top: newPos.toString() + 'px',
			display: 'block', 
			opacity: 0
		});
		
		// Slide and fade to final coordinates
		$(className).animate({
			top: '15px',
			opacity: 0.8
		}, function(){
			// Animation complete, now fadeIn the entire thing
			setTimeout(function() {
				$(className).animate({
					opacity: 1
				});

				// Unet menu opening flag
				menuOpening = false;
				
				// Remove the floating div
				$('#plasmamenu_temp').remove();
			}, 1);
		});
	}
}

/**
* Hide the menu
*/
function hideMenu()
{
	// Stop and reset all instance variables
	clearTimeout(hoverEvent);
	$('div').stop(true, true);
	hoverEvent = null;
	activeTab = null;
	
	// Hide open submenus
	hideSubMenu();
	
	for (var idx = 0; idx < categories.length; idx++)
	{
		// Get class names
		var className = 'mainmenu_' + categories[idx];
		var subClassName = '.mainmenu_' + categories[idx] + '_sub';
		
		// Get the html data from the content and calculate the height of the container
		var htmlData = $('.' + className + '_content').html().replaceAll('\"', '').toLowerCase();
		var entries = htmlData.split('<li class=index');
		var multFactor = entries.length - 1;
		var resetHeight = (isIe ? 27 : 26) * multFactor;
		
		if (!isIe)
		{
			// Set the opacity to 1
			$('.' + className + '_content').css('opacity', 1);
		}
		
		// Reset various container properties
		$('.' + className + '_content').css('height', resetHeight.toString() + 'px');
		$('.' + className + '_animation').html('');
		$('.' + className + '_animation').css('opacity', 0);

		// If we come across a menu that isn't hidden
		if ($(subClassName).css('display') != 'none')
		{
			// Determine destination parameters
			var newPos = $(subClassName).position().top - 40;
			
			// Stop all ongoing animations
			$(subClassName).stop(true, true);
			
			var animProps;

			if (!isIe)
			{
				// Set animation properties
				animProps = {
					top: newPos.toString() + 'px',
					opacity: 0
				};
			}
			else
			{
				// No opacity for IE
				animProps = {top: newPos.toString() + 'px'};				
			}
			
			// Animate and hide the menu
			$(subClassName).animate(animProps, function(){
				$(subClassName).css({
					display: 'none'
				});
			});

		}
	}
	
	// Everything done, now just hide all the other menus, just in case ;-)
	setTimeout(function() {
		for (var idx = 0; idx < categories.length; idx++)
		{
			$('.mainmenu_' + categories[idx] + '_sub').css('display', 'none');
		}
	}, 401);
}

/**
* Slide an open menu
*/
function slideMenu(className)
{	
	// Initialize variables
	var isShown = false;
	var isAnimated = false;

	// Wait till all sliding operations are complete
	var waitLoop = setInterval(function() {
		for (var idx = 0; idx < categories.length; idx++)
		{	
			// Is a menu already shown?
			if ($('.mainmenu_' + categories[idx] + '_sub').css('display') != 'none')
			{
				isShown = true;
			}
			
			// Is a sliding action currently in progress?
			if ($('.mainmenu_' + categories[idx] + '_sub').is(':animated') ||
				$('.mainmenu_' + categories[idx] + '_content').is(':animated') ||
				$('.mainmenu_' + categories[idx] + '_animation').is(':animated'))
			{
				isAnimated = true;
			}
		}
		
		if (isAnimated)
		{
			// Well, this might look stupid, but all it does is continue waiting
			isAnimated = false;
		}
		else
		{
			// Nope, not it's time to do some sliding!
			clearInterval(waitLoop);
			waitLoop = null;
			slideMenuCall(className, isShown);
		}
	}, 10);
}

/**
* Execution function for slideMenu
*/	
function slideMenuCall(className, isShown)
{	
	// Act only when a menu is shown
	if (activeTab != className && isShown)
	{
		// Clear ongoing transitions
		clearTimeout(hoverEvent);
		
		// Hide open submenus
		hideSubMenu();			
		
		// Wait and see if user is hover or just passing by the div
		hoverEvent = setTimeout(function() {
			if (!$('.' + oldTab + '_sub').is(':animated'))
			{
				// Set the current tab
				var oldTab = activeTab;
				activeTab = className;			
				
				// If source != destination
				if (oldTab != activeTab)
				{
					// Determine final X-axis location
					var finalLeft = $('.' + activeTab).position().left - $('.' + oldTab).position().left;
					
					// Determine the old and new heights of the menu
					var oldHeight = $('.' + oldTab + '_content').height();
					var htmlDataNorm = $('.' + activeTab + '_content').html();
					var htmlData = $('.' + activeTab + '_content').html().replaceAll('\"', '').toLowerCase();
					var entries = htmlData.split('<li class=index');
					var multFactor = entries.length - 1;
					var newHeight = (isIe ? 27 : 26) * multFactor;
					
					// Hide the overflow
					$('.menubox_body').css('overflow', 'hidden');
					
					// Add a floating div
					$('body').append("<div id=\"plasmamenu_temp\"></div>");
					$('#plasmamenu_temp').css({
						position: 'absolute',
						height: $('body').height().toString() + 'px',
						width: $('body').width().toString() + 'px',
						zIndex: '9999',
						top: '0px',
						left: '0px',
						cursor: 'pointer'
					});					
					
					// Set the height of the new box to the new height... dirty fix for an unexpected bug
					$('.' + activeTab + '_content').css('height', newHeight.toString() + 'px');
					
					if (!isIe)
					{
						// Fade in the new text
						$('.' + oldTab + '_animation').html(htmlDataNorm);
						$('.' + oldTab + '_animation').css('display', 'block');						
						$('.' + oldTab + '_animation').animate({
							opacity: 1
						});						
						
						// Fade out the old text
						$('.' + oldTab + '_content').animate({
							height: newHeight.toString() + 'px',
							opacity: 0
						});
					}
					
					// Execute the animation effects
					$('.' + oldTab + '_sub').animate({
						left: finalLeft.toString() + 'px'
					}, function() {
						// Hide the old menu completely
						$('.' + oldTab + '_sub').css({
							display: 'none',
							left: '0px'
						});
						
						// Show the new menu immediately
						$('.' + activeTab + '_sub').css({
							display: 'block',
							left: '0px'
						});
						
						// Show the overflow
						$('.menubox_body').css('overflow', 'visible');

						// Remove the temp div
						$('#plasmamenu_temp').remove();
										
						if (!isIe)
						{
							// Reset the text effects for the new_text placeholder
							$('.' + oldTab + '_animation').css({
								display: 'none',
								opacity: 0
							});
							
							// Reset the properties for the actual text as well
							$('.' + oldTab + '_content').css({
								height: oldHeight.toString() + 'px',
								opacity: 1
							});
							
							// Reset the size
							$('.' + activeTab + '_sub').css({
								display: 'block',
								opacity: 1,
								top: '15px'
							});							
						}
					});
				}
			}
		}, 300);
	}
}

/** 
* Show a sub menu
*/
function showSubMenu(divClass)
{
	// Stop all ongoing animations
	$('.' + divClass).stop(true, true);
	
	// Conditional to check if a main menu is sliding
	if (!menuOpening && $('.' + divClass).css('display') == 'none')
	{
		// Wait till openSub isnt null
		var waitLoop = setInterval(function() {
			if (openSub == null)
			{
				// Clear the wait
				clearInterval(waitLoop);
				
				// Mark the open submenu placeholder
				openSub = divClass.substr(0, divClass.length - 4);
				
				// Prepare for animation
				var cssProps, animProps;

				if (!isIe)
				{
					// Build css properties
					cssProps = {
						display: 'block',
						opacity: 0,
						left: '133px'
					};
					
					// Build animation properties
					animProps = {
						opacity: 1,
						left: '173px'
					};
				}
				else
				{	
					// Build IE friendly css properties
					cssProps = {
						display: 'block',
						left: '133px'
					};

					// Build IE friendly animation properties
					animProps = {left: '173px'};					
				}
				
				$('.' + divClass).css(cssProps);
				
				// Animate the submenu
				$('.' + divClass).animate(animProps, 200);
			}
		}, 201);
	}
}

/**
* Hide a submenu
*/
function hideSubMenu()
{
	// Build animation properties
	var animProps;
	
	if (!isIe)
	{
		// Build animation properties
		animProps = {
			left: '133px',
			opacity: 0
		};
	}
	else
	{
		// Build IE friendly animation properties
		animProps = {left: '133px'};
	}

	// Animate and slide the submenu
	$('.' + openSub + '_sub').animate(animProps, 200, function() {

		// Animation complete, lets reset and hide it now
		$('.menubox_subparent').css({
			left: '173px',
			display: 'none'
		});
		
		// Reset openSub placeholder
		openSub = null;	
	});
}

/**
* Track document clicks
*/
function checkClick(e)
{ 
	// Get the target DIV
	var target = (e && e.target) || (event && event.srcElement); 
	
	// Check if target div is outside the menu
	checkParent(target) ? hideMenu() : null; 
} 

/**
* Function to verify the parent of a DIV element
*/
function checkParent(t)
{ 
	while (t.parentNode)
	{ 
		// Get all divs
		var allDivs = document.getElementsByTagName('div');
		
		// Traverse through the div elements
		for (var idx = 0; idx < allDivs.length; idx++)
		{
			// Catch the current div element
			var thisDiv = allDivs[idx];
		
			// If the id is a part of the main_menu category, user is clicking on the menu again
			if ((thisDiv.id.substr(0, 9) == 'mainmenu_' || thisDiv.id == 'plasmamenu_temp') && t == thisDiv)
			{ 
				return false; 
			}
		}		
		
		// Recurse thru children
		t = t.parentNode;
	} 
	
	// User just clicked outside the menu
	return true 
} 

/**
* Replaces all instances of the given substring.
*/
String.prototype.replaceAll = function(strTarget, strSubString) {
	// Initialize variables
	var strText = this;
	
	// Get first match index
	var intIndexOfMatch = strText.indexOf(strTarget);
	 
	// Keep looping till no math is found
	while (intIndexOfMatch != -1)
	{
		// Match found, replace it
		strText = strText.replace(strTarget, strSubString);
		
		// Get next match index
		intIndexOfMatch = strText.indexOf(strTarget);
	}

	// Return the processed string
	return strText;
}
	
/**
* DOM OnReady trigger
*/
document.onready = function() {
	// Detect the user's browser
	detectBrowser();
	
	// Perform all menu related startup stuff
	prepareMenu();
};

/** 
* Invoke the click tracker
*/
document.onclick = checkClick;