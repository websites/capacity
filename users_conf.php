<?php

// configuration file for all contributors


$kde_contributors =array(

'jospoortvliet'=>array(
  'name'=>'Jos Poortvliet',
  'country'=>'Netherlands',
  'city'=>'Utrecht',
  'picture'=>'jospoortvliet.png',
  'blogurl'=>'http://nowwhatthe.blogspot.com',
  'rssurl'=>'http://nowwhatthe.blogspot.com/feeds/posts/default?alt=rss',
  'twitter'=>'jospoortvliet',
  'identica'=>'jospoortvliet',
  'svnaccount'=>'jpoortvliet',
  'opendesktop'=>'jospoortvliet',
  'facebook'=>'jospoortvliet',
  'kdesupportingmember'=>false,
  'gitoriousaccount'=>''
),

'frank'=>array(
  'name'=>'Frank Karlitschek',
  'country'=>'Germany',
  'city'=>'Stuttgart',
  'picture'=>'karli.png',
  'blogurl'=>'http://blog.karlitschek.de',
  'rssurl'=>'http://blog.karlitschek.de/feeds/posts/default?alt=rss',
  'twitter'=>'fkarlitschek',
  'identica'=>'fkarlitschek',
  'svnaccount'=>'karli',
  'opendesktop'=>'frank',
  'facebook'=>'karlitschek',
  'kdesupportingmember'=>false,
  'gitoriousaccount'=>'karlitschek'
),

'fregl'=>array(
  'name'=>'Frederik Gladhorn',
  'country'=>'Germany',
  'city'=>'Stuttgart',
  'picture'=>'fregl.jpg',
  'blogurl'=>'http://blogs.fsfe.org/gladhorn/',
  'rssurl'=>'http://blogs.fsfe.org/gladhorn/feed/',
  'twitter'=>'fregl',
  'identica'=>'frederik',
  'svnaccount'=>'fregl',
  'opendesktop'=>'fregl',
  'facebook'=>'gladhorn',
  'kdesupportingmember'=>false,
  'gitoriousaccount'=>''
),

'nightrose'=>array(
  'name'=>'Lydia Pintscher',
  'country'=>'Germany',
  'city'=>'Karlsruhe',
  'picture'=>'nightrose.png',
  'blogurl'=>'http://blog.lydiapintscher.de/',
  'rssurl'=>'http://blog.lydiapintscher.de/feed/',
  'twitter'=>'nightrose',
  'identica'=>'nightrose',
  'svnaccount'=>'lydia',
  'opendesktop'=>'nightrose',
  'facebook'=>'lydiapintscher',
  'kdesupportingmember'=>false,
  'gitoriousaccount'=>'nightrose'
),

'einar'=>array(
  'name'=>'Luca Beltrame',
  'country'=>'Italy',
  'city'=>'Milano',
  'picture'=>'einar.png',
  'blogurl'=>'http://www.dennogumi.org',
  'rssurl'=>'http://www.dennogumi.org/category/kde/feed',
  'twitter'=>'gstarwind',
  'identica'=>'gstarwind',
  'svnaccount'=>'lbeltrame',
  'opendesktop'=>'einar77',
  'facebook'=>'',
  'kdesupportingmember'=>false,
  'gitoriousaccount'=>''
),

'foobar'=>array(
  'name'=>'Fooo',
  'country'=>'Germany',
  'city'=>'Stuttgart',
  'picture'=>'foo.png',
  'blogurl'=>'',
  'rssurl'=>'',
  'twitter'=>'',
  'identica'=>'',
  'svnaccount'=>'',
  'opendesktop'=>'',
  'facebook'=>'',
  'kdesupportingmember'=>false,
  'gitoriousaccount'=>''
),

'aseigo'=>array(
    'name'=>'Aaron Seigo',
    'country'=>'Canada',
    'city'=>'Vancouver',
    'picture'=>'aseigo.png',
    'blogurl'=>'http://aseigo.blogspot.com',
    'rssurl'=>'http://aseigo.blogspot.com/rss.xml',
    'twitter'=>'',
    'identica'=>'aseigo',
    'svnaccount'=>'aseigo',
    'opendesktop'=>'aseigo',
    'facebook'=>'aseigo',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>'aseigo'
),

'sebas'=>array(
    'name'=>'Sebastian Kuegler',
    'country'=>'Netherlands',
    'city'=>'Nijmegen',
    'picture'=>'sebas.png',
    'blogurl'=>'http://vizZzion.org',
    'rssurl'=>'http://vizZzion.org/blog/feed/',
    'twitter'=>'sebasje',
    'identica'=>'sebas',
    'svnaccount'=>'sebas',
    'opendesktop'=>'sebas',
    'facebook'=>'',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>'sebas'
),

'Pinheiro'=>array(
    'name'=>'Nuno Pinheiro',
    'country'=>'Portugal',
    'city'=>'Vila Real',
    'picture'=>'pinheiro.png',
    'blogurl'=>'http://pinheiro-kde.blogspot.com/',
    'rssurl'=>'http://pinheiro-kde.blogspot.com/feeds/posts/default?alt=rss',
    'identica'=>'pinheirokde',
    'svnaccount'=>'pinheiro',
    'opendesktop'=>'nunopinheirokde',
    'facebook'=>'nuno.pinheiro1',
    'kdesupportingmember'=>false, 
    'gitoriousaccount'=>''
),

'zx2c4'=>array(
    'name'=>'Jason A. Donenfeld',
    'country'=>'United States',
    'city'=>'New York',
    'picture'=>'zx2c4.png',
    'blogurl'=>'http://blog.jasondonenfeld.com',
    'rssurl'=>'http://blog.jasondonenfeld.com/planetkde',
    'twitter'=>'zx2c4',
    'identica'=>'',
    'svnaccount'=>'jdonenfeld',
    'opendesktop'=>'zx2c4',
    'facebook'=>'profile.php?id=1414830056',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>'zx2c4'
),

'apachelogger'=>array(
    'name'=>'Harald Sitter',
    'country'=>'Austria',
    'city'=>'Graz',
    'picture'=>'apachelogger.png',
    'blogurl'=>'http://apachelog.wordpress.com',
    'rssurl'=>'http://apachelog.wordpress.com/tag/kde/feed/?mrss=off',
    'twitter'=>'apachelogger',
    'identica'=>'apachelogger',
    'svnaccount'=>'sitter',
    'opendesktop'=>'apachelogger',
    'facebook'=>'apachelogger',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>'apachelogger'
),

'stuart'=>array(
    'name'=>'Stuart Jarvis',
    'country'=>'Great Britain',
    'city'=>'Southampton',
    'picture'=>'',
    'blogurl'=>'http://www.asinen.org',
    'rssurl'=>'http://www.asinen.org/feed',
    'twitter'=>'swjarvis',
    'identica'=>'swjarvis',
    'svnaccount'=>'',
    'opendesktop'=>'stuartjarvis',
    'facebook'=>'stuartwjarvis',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>''
),

'justin'=>array(
    'name'=>'Justin Kirby',
    'country'=>'United States',
    'city'=>'Washington DC',
    'picture'=>'justin.jpeg',
    'blogurl'=>'http://neomantra.org/',
    'rssurl'=>'http://neomantra.org/?feed=rss2',
    'twitter'=>'neomantra82',
    'identica'=>'neomantra',
    'svnaccount'=>'',
    'opendesktop'=>'neomantra',
    'facebook'=>'neomantra',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>''
),

'cullmann'=>array(
    'name'=>'Christoph Cullmann',
    'country'=>'Germany',
    'city'=>'Saarbruecken',
    'picture'=>'',
    'blogurl'=>'http://kate-editor.org/author/cullmann/',
    'rssurl'=>'http://kate-editor.org/author/cullmann/feed/?tag=planet',
    'twitter'=>'',
    'identica'=>'',
    'svnaccount'=>'cullmann',
    'opendesktop'=>'',
    'facebook'=>'',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>'cullmann'
),

'dhaumann'=>array(
    'name'=>'Dominik Haumann',
    'country'=>'Germany',
    'city'=>'Darmstadt',
    'picture'=>'',
    'blogurl'=>'http://kate-editor.org/author/haumann/',
    'rssurl'=>'http://kate-editor.org/author/haumann/feed/?tag=planet',
    'twitter'=>'',
    'identica'=>'',
    'svnaccount'=>'dhaumann',
    'opendesktop'=>'',
    'facebook'=>'',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>'dhaumann'
),

'fmontesi'=>array(
    'name'=>'Fabrizio Montesi',
    'country'=>'Italy',
    'city'=>'Bologna',
    'picture'=>'fmontesi.png',
    'blogurl'=>'http://fmontesi.blogspot.com/',
    'rssurl'=>'http://fmontesi.blogspot.com/rss.xml',
    'twitter'=>'',
    'identica'=>'fmontesi',
    'svnaccount'=>'fmontesi',
    'opendesktop'=>'fmontesi',
    'facebook'=>'',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>''
),

'gamaral'=>array(
    'name'=>'Guillermo Amaral',
    'country'=>'Mexico',
    'city'=>'Tijuana',
    'picture'=>'gamaral.jpg',
    'blogurl'=>'http://ga.maral.me/',
    'rssurl'=>'http://feeds.feedburner.com/gamaral-kde',
    'twitter'=>'gamaral',
    'identica'=>'gamaral',
    'svnaccount'=>'gamaral',
    'opendesktop'=>'gamaral',
    'facebook'=>'g4m4r4l',
    'kdesupportingmember'=>false,
    'gitoriousaccount'=>'gamaral'
), 

'sayakb'=>array(
  'name'=>'Sayak Banerjee',
  'country'=>'India',
  'city'=>'Gurgaon',
  'picture'=>'sayakb.png',
  'blogurl'=>'http://www.sayakbanerjee.com',
  'rssurl'=>'http://www.sayakbanerjee.com/category/planet-kde/feed/',
  'twitter'=>'sayakb',
  'identica'=>'sayakb',
  'svnaccount'=>'sayakb',
  'opendesktop'=>'sayakb',
  'facebook'=>'sayakban',
  'kdesupportingmember'=>false,
  'gitoriousaccount'=>'sayakb'
),

'unormal'=>array(
  'name'=>'Mario Fux',
  'country'=>'Switzerland',
  'city'=>'Burgdorf',
  'picture'=>'',
  'blogurl'=>'http://blogs.fsfe.org/mario/',
  'rssurl'=>'http://blogs.fsfe.org/mario/?feed=rss2&tag=kde',
  'twitter'=>'',
  'identica'=>'',
  'svnaccount'=>'fux',
  'opendesktop'=>'',
  'facebook'=>'',
  'kdesupportingmember'=>false,
  'gitoriousaccount'=>'fux'
)

);

?>
