# Translation of media_www to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2020, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2023-03-11 23:28+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: includes/aether/template-bottom2.inc:5
#: includes/chihuahua/template-bottom2.inc:15
#: includes/derry/template-bottom2.inc:5
#: includes/layout_flatblue/template-bottom2.inc:5
#: includes/newlayout/template-bottom2.inc:5 includes/template-bottom2.inc:52
msgid "Maintained by"
msgstr "Vedlikehalde av"

#: includes/aether/template-bottom2.inc:7
#: includes/chihuahua/template-bottom2.inc:17
#: includes/derry/template-bottom2.inc:7
#: includes/layout_flatblue/template-bottom2.inc:7
#: includes/newlayout/template-bottom2.inc:7
msgid ""
"Maintained by <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;"
"ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">The KDE Webmaster</a><br />\n"
msgstr ""
"Vedlikehalde av <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;"
"e&#098;ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">nettansvarleg for KDE</"
"a><br />\n"

#: includes/aether/template-bottom2.inc:13
#: includes/aether/template-bottom2.inc:26
#: includes/aether/template-bottom2.inc:44
#: includes/layout_flatblue/template-bottom2.inc:13
#: includes/layout_flatblue/template-bottom2.inc:26
#: includes/layout_flatblue/template-bottom2.inc:44
#: includes/newlayout/template-bottom2.inc:13
#: includes/newlayout/template-bottom2.inc:26
#: includes/newlayout/template-bottom2.inc:44 includes/search.inc:2
msgid "Search:"
msgstr "Søk:"

#: includes/aether/template-bottom2.inc:28
#: includes/aether/template-bottom2.inc:46 includes/derry/template-top1.inc:47
#: includes/layout_flatblue/template-bottom2.inc:28
#: includes/layout_flatblue/template-bottom2.inc:46
#: includes/newlayout/template-bottom2.inc:28
#: includes/newlayout/template-bottom2.inc:46 includes/search.inc:15
#: includes/template-bottom2.inc:10
msgid "Search"
msgstr "Søk"

#: includes/aether/template-bottom2.inc:57
#: includes/layout_flatblue/template-bottom2.inc:57
#: includes/newlayout/template-bottom2.inc:57
msgid "Language:"
msgstr "Språk:"

#: includes/aether/template-bottom2.inc:72
#: includes/chihuahua/template-top1.inc:175 includes/derry/toolbox.inc:16
#: includes/layout_flatblue/template-bottom2.inc:72
#: includes/newlayout/template-bottom2.inc:72
msgid "Change language"
msgstr "Byt språk"

#: includes/aether/template-bottom2.inc:85
#: includes/layout_flatblue/template-bottom2.inc:85
#: includes/newlayout/template-bottom2.inc:85
msgid "Donate:"
msgstr "Gje pengegåve:"

#: includes/aether/template-bottom2.inc:85
#: includes/chihuahua/template-top1.inc:140
#: includes/layout_flatblue/template-bottom2.inc:85
#: includes/newlayout/template-bottom2.inc:85
msgid "(Why?)"
msgstr "(Korfor?)"

#: includes/aether/template-bottom2.inc:88
#: includes/chihuahua/template-top1.inc:142
#: includes/layout_flatblue/template-bottom2.inc:88
#: includes/newlayout/template-bottom2.inc:88
msgid ""
"Your donation is smaller than %1€. This means that most of your "
"donation\\nwill end up in processing fees. Do you want to continue?"
msgstr ""
"Gåva di er mindre enn %1 euro. Dette gjer at du det meste\n"
"av gåva går vekk i gebyr. Vil du likevel halda fram?"

#: includes/aether/template-bottom2.inc:99
#: includes/chihuahua/template-top1.inc:153
#: includes/layout_flatblue/template-bottom2.inc:99
#: includes/newlayout/template-bottom2.inc:99
msgid "Donate"
msgstr "Gje pengegåve"

#: includes/aether/template-bottom2.inc:112
#: includes/layout_flatblue/template-bottom2.inc:116
#: includes/newlayout/template-bottom2.inc:116
msgid ""
"KDE<sup>&#174;</sup> and <a href=\"/media/images/"
"trademark_kde_gear_black_logo.png\">the K Desktop Environment<sup>&#174;</"
"sup> logo</a> are registered trademarks of "
msgstr ""
"KDE<sup>&#174;</sup> og <a href=\"/media/images/"
"trademark_kde_gear_black_logo.png\">K Desktop Environment<sup>&#174;</sup>-"
"logoen</a> er registrerte varmerke tilhøyrande "

#: includes/aether/template-bottom2.inc:114
#: includes/chihuahua/template-bottom2.inc:24
#: includes/derry/template-bottom2.inc:13
#: includes/layout_flatblue/template-bottom2.inc:118
#: includes/newlayout/template-bottom2.inc:118 includes/template-bottom2.inc:67
msgid "Legal"
msgstr "Juridisk"

#: includes/chihuahua/template-bottom2.inc:23
#: includes/derry/template-bottom2.inc:12
msgid ""
"KDE<sup>&#174;</sup> and <a href=\"/media/images/"
"trademark_kde_gear_black_logo.png\">the K Desktop Environment<sup>&#174;</"
"sup> logo</a> are registered trademarks of <a href=\"http://ev.kde.org/\" "
"title=\"Homepage of the KDE non-profit Organization\">KDE e.V.</a>"
msgstr ""
"KDE<sup>&#174;</sup> og <a href=\"/media/images/"
"trademark_kde_gear_black_logo.png\">K Desktop Environment<sup>&#174;</sup>-"
"logoen</a> er registrerte varemerke tilhøyrande <a href=\"http://ev.kde.org/"
"\" title=\"Homepage of the KDE non-profit Organization\">KDE e.V.</a>"

#: includes/chihuahua/template-top1.inc:140
msgid "DONATE"
msgstr "PENGEGÅVE"

#: includes/classes/class_appdata.inc:42
msgid "%1 is unmaintained and no longer released by the KDE community."
msgstr ""
"%1 er ikkje lenger vedlikehalde og vert ikkje lenger utgjeve av KDE-"
"fellesskapet."

#: includes/classes/class_appdata.inc:85
msgid "Authors:"
msgstr "Utviklarar:"

#: includes/classes/class_appdata.inc:100
msgid "Thanks To:"
msgstr "Takk til:"

#: includes/classes/class_appdata.inc:127
msgid ""
"%1 is distributed under the terms of the <a href=\"http://www.gnu.org/"
"licenses/old-licenses/gpl-2.0.html\">GNU General Public License (GPL), "
"Version 2</a>."
msgstr ""
"%1 vert distribuert under vilkåra i <a href=\"https://www.gnu.org/licenses/"
"old-licenses/gpl-2.0.html\">GNU General Public License (GPL), versjon 2</a>."

#: includes/classes/class_appdata.inc:130
msgid ""
"%1 is distributed under the terms of the <a href=\"http://www.gnu.org/"
"licenses/gpl.html\">GNU General Public License (GPL), Version 3</a>."
msgstr ""
"%1 vert distribuert under vilkåra i <a href=\"https://www.gnu.org/licenses/"
"gpl.html\">GNU General Public License (GPL), versjon 3</a>."

#: includes/classes/class_appdata.inc:133
msgid ""
"%1 is distributed under the terms of the <a href=\"http://www.gnu.org/"
"licenses/old-licenses/lgpl-2.0.html\"> GNU Library General Public License, "
"version 2</a>."
msgstr ""
"%1 vert distribuert under vilkåra i <a href=\"https://www.gnu.org/licenses/"
"old-licenses/lgpl-2.0.html\">GNU Library General Public License, versjon 2</"
"a>."

#: includes/classes/class_appdata.inc:213
msgid "Browse %1 source code on WebSVN"
msgstr "Sjå kjeldekoden til %1 på WebSVN"

#: includes/classes/class_appdata.inc:216
msgid "Browse %1 source code online"
msgstr "Sjå kjeldekoden til %1 på nettet"

#: includes/classes/class_appdata.inc:224
msgid "Checkout source code:"
msgstr "Sjekk ut kjeldekoden:"

#: includes/classes/class_appdata.inc:226
msgid "Clone %1 source code:"
msgstr "Kloen kjeldekoden til %1:"

#: includes/classes/class_appinfo.inc:95
msgid ""
"%1 is distributed under the terms of the <a href=\"http://www.gnu.org/"
"licenses/gpl.html\">GNU General Public License (GPL), Version 2</a>."
msgstr ""
"%1 vert distribuert under vilkåra i <a href=\"https://www.gnu.org/licenses/"
"gpl.html\">GNU General Public License (GPL), versjon 2</a>."

#: includes/classes/class_appinfo.inc:100
msgid ""
"%1 is distributed under the terms of the <a href=\"http://www.gnu.org/"
"licenses/lgpl.html\">GNU Lesser General Public License (LGPL), Version 2</a>."
msgstr ""
"%1 vert distribuert under vilkåra i <a href=\"http://www.gnu.org/licenses/"
"lgpl.html\">GNU Lesser General Public License (LGPL), versjon 2</a>."

#: includes/classes/class_appinfo.inc:104
msgid ""
"Unknown license, valid licenses for setLicense() are 'gpl', 'lgpl'. Update "
"www/media/includes/classes/class_appinfo.inc if another is required."
msgstr ""
"Ukjend lisens. Gyldige lisensar for setLicense() er «gpl» og «lgpl». "
"Oppdater www/media/includes/classes/class_appinfo.inc dersom du treng støtte "
"for ein annan lisens."

#: includes/classes/class_appinfo.inc:114
msgid "<b>Version %1</b>"
msgstr "<b>Versjon %1</b>"

#: includes/classes/class_faq.inc:39
msgid "Questions"
msgstr "Spørsmål"

#: includes/classes/class_faq.inc:59
msgid "Answers"
msgstr "Svar"

#: includes/classes/class_faq.inc:95
msgid "[Up to Questions]"
msgstr "[Opp til spørsmål]"

#: includes/classes/class_menu.inc:177
msgid "Skip menu \"%1\""
msgstr "Hopp over menyen «%1»"

#: includes/dotandapps.inc:32 includes/functions.inc:82 includes/rss2.inc:37
msgid "Latest News"
msgstr "Siste nytt"

#: includes/dotandapps.inc:44 includes/dotandapps.inc:97
#: includes/functions.inc:113 includes/rss2.inc:67
msgid "Date"
msgstr "Dato"

#: includes/dotandapps.inc:44 includes/functions.inc:113 includes/rss2.inc:67
msgid "Headline"
msgstr "Overskrift"

#: includes/dotandapps.inc:44
msgid "This is a list of the latest news headlines shown on KDE Dot News"
msgstr ""
"Her er ei oversikt over dei siste nyheits­overskriftene frå KDE Dot News"

#: includes/dotandapps.inc:61
msgid "Read the full article on dot.kde.org"
msgstr "Les heile artikkelen på dot.kde.org"

#: includes/dotandapps.inc:69
msgid "View older news items"
msgstr "Vis eldre nyheiter"

#: includes/dotandapps.inc:69
msgid "more news..."
msgstr "fleire nyheiter …"

#: includes/dotandapps.inc:84
msgid "Latest Applications"
msgstr "Nyasteprogram"

#: includes/dotandapps.inc:96
msgid "List of latest third party application releases for KDE"
msgstr "Oversikt over siste utgjevingar av tredjeparts-program frå KDE"

#: includes/dotandapps.inc:98
msgid "Application / Release"
msgstr "Program/utgåver"

#: includes/dotandapps.inc:193
msgid "View <a href=\"http://kde-apps.org/\">more applications...</a>"
msgstr "Sjå <a href=\"http://kde-apps.org/\">fleire program …</a>"

#: includes/dotandapps.inc:193
msgid "Disclaimer: "
msgstr "Ansvarsfråskriving: "

#: includes/dotandapps.inc:193
msgid ""
"Application feed provided by <a href=\"http://kde-apps.org/\">kde-apps.org</"
"a>, an independent KDE website."
msgstr ""
"Programoversikta kjem frå <a href=\"https://kde-apps.org/\">kde-apps.org</"
"a>, ein uavhengig KDE-nettstad."

#: includes/footer.inc:27
msgid "Global navigation links"
msgstr "Globale navigeringslenkjer"

#: includes/footer.inc:29
msgid "KDE Home"
msgstr "KDE-heimeside"

#: includes/footer.inc:30
msgid "KDE Accessibility Home"
msgstr "Heimeside for KDE Accessibility"

#: includes/footer.inc:31
msgid "Description of Access Keys"
msgstr "Skildring av snøggtastar"

#: includes/footer.inc:32
msgid "Back to content"
msgstr "Tilbake til innhaldet"

#: includes/footer.inc:33
msgid "Back to menu"
msgstr "Tilbake til menyen"

#: includes/header.inc:101
msgid "K Desktop Environment"
msgstr "K Desktop Environment"

#: includes/header.inc:148
msgid "Skip to content"
msgstr "Hopp til innhaldet"

#: includes/header.inc:149
msgid "Skip to link menu"
msgstr "Hopp til lenkjemenyen"

# Veit ikkje om ein kan bruka andre teikn her.
#: includes/rss2.inc:85
msgid "Y-m-d"
msgstr "d-m-Y"

#: includes/search.inc:5
msgid "Select what or where you want to search"
msgstr "Vel kva eller kor du vil søkja"

#: includes/search.inc:7
msgid "kde.org"
msgstr "kde.org"

#: includes/search.inc:9
msgid "developer.kde.org"
msgstr "developer.kde.org"

#: includes/search.inc:11
msgid "kde-look.org"
msgstr "kde-look.org"

#: includes/search.inc:12
msgid "Applications"
msgstr "Program"

#: includes/search.inc:13
msgid "Documentation"
msgstr "Dokumentasjon"

#: includes/template-bottom2.inc:11
msgid "Hotspot"
msgstr "Hovudområde"

#: includes/template-bottom2.inc:66
msgid ""
"KDE<sup>&#174;</sup> and <a href=\"/media/images/kde_gear_black.png\">the K "
"Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of "
"<a href=\"http://ev.kde.org/\" title=\"Homepage of the KDE non-profit "
"Organization\">KDE e.V.</a>"
msgstr ""
"KDE<sup>&#174;</sup> og <a href=\"/media/images/kde_gear_black.png\">K "
"Desktop Environment<sup>&#174;</sup>-logoen</a> er registrerte varemerke til "
"<a href=\"http://ev.kde.org/\" title=\"Heimesida til den ideelle "
"organisasjonen KDE e.V.\">KDE e.V.</a>"

#: includes/template-top1.inc:21
msgid "A complete structural overview of the KDE.org web pages"
msgstr "Fullstendig oversikt over strukturen til KDE.org-nettsidene"

#: includes/template-top1.inc:21
msgid "Sitemap"
msgstr "Nettstad-oversikt"

#: includes/template-top1.inc:22
msgid "Having problems? Read the documentation"
msgstr "Problem? Les dokumentasjonen."

#: includes/template-top1.inc:22
msgid "Help"
msgstr "Hjelp"

#: includes/template-top1.inc:23
msgid "Contact information for all areas of KDE"
msgstr "Kontaktinformasjon for alle KDE-område"

#: includes/template-top1.inc:23
msgid "Contact Us"
msgstr "Ta kontakt"

#: includes/template-top1.inc:26
msgid "Location"
msgstr "Stad"
